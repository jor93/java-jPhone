/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 29.04.2015
 * Datei: GlobalSettings.java
 */
package settings;

import java.awt.Color;
import java.awt.Font;

/**
 * @author jor
 *
 */
public class GlobalSettings {

	public static boolean debugMode = true;
	public static boolean broMode = false;

	//******************************************
	//Dimension
	//******************************************
	//
	//jPhone
	public static int width = 455;
	public static int height = 690;
	//ControlPanel
	public static int widthPanel = 460;
	public static int heightPanel = 325;
	//Apps
	public static int widthApp = 100;
	//Controls
	public static int heigthControl = 75;

	//******************************************
	//Font
	//******************************************
	//
	//Global Fonts
	public static Font globalFont(int fontSize){
		Font globalFont = new Font("Arial", Font.PLAIN, fontSize);
		return globalFont;
	}
	public static Font globalFontItalic(int fontSize){
		Font globalFont = new Font("Arial", Font.ITALIC, fontSize);
		return globalFont;
	}

	//******************************************
	//Backgrounds
	//******************************************
	//
	//LockScreen
	public static String backgroundImage1 = "./img/bg/background1.png";
	public static String backgroundImage2 = "./img/bg/background2.png";
	public static String backgroundImage3 = "./img/bg/background3.png";
	public static String backgroundImage4 = "./img/bg/background4.png";
	//Sudoku
	public static String sudokuBackground = "./img/sudoku/sudoku.png";
	public static String sudokuBackgroundText = "./img/sudoku/sudoku_text.png";

	//******************************************
	//Icons
	//******************************************
	//
	//Controls
	public static String back = "./img/controls/back.png";
	public static String home = "./img/controls/home.png";
	public static String lock = "./img/controls/lock.png";
	public static String brofist = "./img/controls/fist.png";
	//HomeScreen
	public static String contact = "./img/contact/icon.png";
	public static String calculator = "./img/calculator/icon.png";
	public static String sms = "./img/sms/icon.png";
	public static String smsNotification = "./img/sms/notification.png";
	public static String sudoku = "./img/sudoku/icon.png";
	public static String settings = "./img/settings/icon.png";
	//Sample
	public static String sampleViewSearch = "./img/sample/search.png";
	public static String sampleViewDelete = "./img/sample/delete.png";
	public static String sampleViewRemove = "./img/sample/remove.png";
	//Contact
	public static String contactSave = "./img/contact/save.png";
	public static String contactDelete = "./img/contact/delete.png";

	//******************************************
	//Colors
	//******************************************
	//
	//Transperency
	public static Color transparentWhite(int transperency){
		Color transparentWhite = new Color(255,255,255,transperency);
		return transparentWhite;
	}
	public static Color transparentBlack(int transperency){
		Color transparentBlack = new Color(0,0,0,transperency);
		return transparentBlack;
	}
	//Font Colors
	public static Color globalFontColor(){
		return Color.WHITE;
	}
	public static Color globalFontColorBlack(){
		return Color.BLACK;
	}
	//Controls
	public static Color colorControls(){
		Color fontControls = new Color(80,80,80);
		return fontControls;
	}
	//LockScreen 
	public static Color newMessage(){
		Color newMessage = new Color(88,152,237);
		return newMessage;
	}
	public static Color newMessageBright(){
		Color newMessageBright = new Color(239,83,80);
		return newMessageBright;
	}
	public static Color newMessageDark(){
		Color newMessageBright = new Color(198,40,40);
		return newMessageBright;
	}
	// Renderer
	public static Color rendererNumber(){
		Color sampleNumber = new Color(0,191,255);
		return sampleNumber;
	}
	public static Color rendererBottom(){
		Color rendererBottom = new Color(150,150,150);
		return rendererBottom;
	}
	public static Color rendererList2(){
		Color rendererList2 = new Color(230,230,230);
		return rendererList2;
	}
	//Calculator
	public static Color calculatorOperators(){
		Color calculatorOperators = new Color(220,220,225);
		return calculatorOperators;
	}
	public static Color calculatorResult(){
		Color calculatorResult = new Color(0,153,0);
		return calculatorResult;
	}

	//Sudoku
	public static Color sudokuHover(){
		Color sudokuHover = new Color(179,198,255);
		return sudokuHover;
	}

	public static Color sudokuVerifier(){
		Color sudokuVerifier = new Color(255,100,100);
		return sudokuVerifier;
	}

	//Contacts
	public static Color contactMainColor(){
		Color calculatorMain = new Color(184,9,83);
		return calculatorMain;
	}

	public static Color contactSave(){
		Color contactSave = new Color(67,160,71);
		return contactSave;
	}

	public static Color contactDelete(){
		Color contactDelete = new Color(245,60,60);
		return contactDelete;
	}
	//SMS
	public static Color smsMainColor(){
		Color smsMainColor = new Color(56,142,60);
		return smsMainColor;
	}
	//Settings
	public static Color settingsMainColor(){
		Color settingsMain = new Color(117,117,117);
		return settingsMain;
	}
}
