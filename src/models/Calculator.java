/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 05.05.2015
 * Datei: Calculator.java
 */
package models;

/**
 * @author jor
 *
 */
public class Calculator {

	public static double calculate(String s, boolean minus){
		double result = 0;
		if(s.contains("�") || s.contains("�") || s.contains("+") || s.contains("-")){
			String [] array = s.split("[�|�|+]\\-|[�|�|\\-|+]");
			if(s.contains("�") == true)
				s = s.replaceAll("�", "/");
			if(s.contains("�") == true)
				s = s.replaceAll("�", "*");
			if(s.contains(".") == true)
				s = s.replaceAll("\\.", ",");
			String [] operators = s.split("([0-9]*,[0-9]*)|[0-9]+");
			operators[0] = (minus ? "-" : "+") ; 
			double [] digits = new double[array.length];
			for (int i = 0; i < digits.length; i++) {
				digits[i] = Double.parseDouble(array[i]);
			}
			if(s.contains("/") || s.contains("*")){
				int countPoint = s.length()-s.replaceAll("((/\\-)|(/)|(\\*\\-)|(\\*))", "").length();
				double [] resultsPoint = new double[countPoint]; 
				int position = 0;
				for (int i = 1; i < digits.length; i++) {
					if(operators[i].matches("((/\\-)|(/)|(\\*\\-)|(\\*))") == true && operators[i-1].matches("((/\\-)|(/)|(\\*\\-)|(\\*))") != true)
					{
						switch (operators[i]) {
						case "/":
							resultsPoint[position] =((operators[i-1].compareToIgnoreCase("-")==0) ? -digits[i-1] : +digits[i-1]) / digits[i];
							break;
						case "*":
							resultsPoint[position] = ((operators[i-1].compareToIgnoreCase("-")==0) ? -digits[i-1] : +digits[i-1]) * digits[i];
							break;
						case "/-":
							resultsPoint[position] = ((operators[i-1].compareToIgnoreCase("-")==0) ? -digits[i-1] : +digits[i-1]) / -digits[i];
							break;
						case "*-":
							resultsPoint[position] = ((operators[i-1].compareToIgnoreCase("-")==0) ? -digits[i-1] : +digits[i-1]) * -digits[i];
							break;
						}
						position++;
					} else {
						switch (operators[i]) {
						case "/":
							resultsPoint[position-1] =((operators[i].compareToIgnoreCase("-")==0) ? -resultsPoint[position-1] : +resultsPoint[position-1]) / digits[i];
							break;
						case "*":
							resultsPoint[position-1] = ((operators[i].compareToIgnoreCase("-")==0) ? -resultsPoint[position-1] : +resultsPoint[position-1]) * digits[i];
							break;
						case "/-":
							resultsPoint[position-1] = ((operators[i].compareToIgnoreCase("-")==0) ? -resultsPoint[position-1] : +resultsPoint[position-1]) / digits[i];
							break;
						case "*-":
							resultsPoint[position-1] = ((operators[i].compareToIgnoreCase("-")==0) ? -resultsPoint[position-1] : +resultsPoint[position-1]) * -digits[i];
							break;
						}
					}
				}
				position = 0;
				for (int i=0; i < digits.length; i++) {
					if(i != digits.length-1){
						if(operators[i+1].compareToIgnoreCase("/") == 0 || operators[i+1].compareToIgnoreCase("*") == 0 || operators[i+1].compareToIgnoreCase("*-") == 0 || operators[i+1].compareToIgnoreCase("/-") == 0){
							result += resultsPoint[position];
							position++;
							continue;
						}
					}
					switch (operators[i]) {
					case "+":
						result += digits[i];
						break;
					case "-":
						result -= digits[i];
						break;
					case "+-":
						result += -digits[i];
						break;
					}
				}
				return result;
			}else return plusMinusCalculation(operators,digits);
		}else{
			result = (minus ? -Double.parseDouble(s) :Double.parseDouble(s));
			return result;
		}	
	}

	private static double plusMinusCalculation(String [] operators, double [] digits){
		double result = 0;
		for (int i=0; i < digits.length; i++) {
			switch (operators[i]) {
			case "+":
				result += digits[i];
				break;
			case "-":
				result -= digits[i];
				break;
			case "+-":
				result += -digits[i];
				break;
			}
		}
		return result;	
	}
}