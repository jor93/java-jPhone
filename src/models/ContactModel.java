/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 27.04.2015
 * Datei: ContactModel.java
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author jor
 *
 */
public class ContactModel implements Serializable{

	private static final long serialVersionUID = 2306111111587817152L;
	private List<Contact> contactList;
	
	public List<Contact> getContacts(){
		return contactList;
	}
	
	public ContactModel() {
		contactList = new ArrayList<Contact>();
	}
	
	public Contact [] getContactList() {
		return contactList.toArray(new Contact[contactList.size()]);
	}

	public void setContactList(List<Contact> contactList) {
		this.contactList = contactList;
	}

	public boolean alreadyInList(String phoneNumber){
		for (Contact e : contactList) {
			if(e.equalsPhoneNumber(phoneNumber) == true)
				return true;
		}
		return false;
	}
	
	public Contact getContactByPhone(String phone) {
		Contact contact = new Contact();
		contact.setPhoneNumber(phone);
		for(Contact e : contactList) {
			if(e.getPhoneNumber().equals(phone)) {
				contact = e;
			}
		}
		return contact;
	}
	
	public void addContact(Contact c) {
		boolean adding = true;
		for(Contact e : contactList) {
			if(e.equals(c)) {
				adding = false;
				return;
			}
		}
		if(adding) {
			contactList.add(c);
			sort();
		}
	}
		
	public void removeContact(Contact c){
		int index = -1;
		int counter = 0;
		for(Contact e : contactList) {
			if(e.equals(c)) {
				index = counter;
			}
			counter++;
		}
		contactList.remove(index);
		sort();
	}
	
	public void updateContact(Contact old, Contact c) {
		int index = -1;
		int counter = 0;
		for(Contact e : contactList) {
			if(e.equals(old)) {
				index = counter;
			}
			counter++;
		}
		contactList.set(index, c);
		sort();
	}
	
	private void sort(){
		Collections.sort(contactList, new Comparator<Contact>() {
			@Override
			public int compare(Contact o1, Contact o2) {
				return o1.getLname().compareTo(o2.getLname());
			}
		});
	}
	
	public Contact [] search(String s){
		List<Contact> result = contactList.stream().filter((Contact c) -> (c.getLname().concat(" ").concat(c.getFname())).toLowerCase().startsWith(s.toLowerCase()) || c.getFname().toLowerCase().startsWith(s.toLowerCase())).collect(Collectors.toList());
		return result.toArray(new Contact[result.size()]);
	}
	
}
