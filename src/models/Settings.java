/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 24.05.2015
 * Datei: Settings.java
 */
package models;

import java.awt.Color;
import java.io.Serializable;
import settings.GlobalSettings;

/**
 * @author jor
 *
 */
public class Settings implements Serializable{
	private static final long serialVersionUID = 6128511818409141390L;

	private String phoneNumber;
	
	private boolean debugMode;
	private final boolean DEBUGMODEDEFAULT = GlobalSettings.debugMode;
	private boolean broMode;
	private final boolean BROMODEDEFAULT = GlobalSettings.broMode;
	
	private Color controlsColor;
	private final Color CONTROLSCOLORDEFAULT = GlobalSettings.colorControls();
	private Color smsMainColor;
	private final Color SMSMAINCOLORDEFAULT = GlobalSettings.smsMainColor();
	private Color contactMainColor;
	private final Color CONTACTMAINCOLORDEFAULT = GlobalSettings.contactMainColor();
	private Color settingsMainColor;
	private final Color SETTINGSMAINCOLORDEFAULT = GlobalSettings.settingsMainColor();
	
	public Settings() {
		super();
		this.debugMode = DEBUGMODEDEFAULT;
		this.broMode = BROMODEDEFAULT;
		this.controlsColor = CONTROLSCOLORDEFAULT;
		this.smsMainColor = SMSMAINCOLORDEFAULT;
		this.contactMainColor = CONTACTMAINCOLORDEFAULT;
		this.settingsMainColor = SETTINGSMAINCOLORDEFAULT;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public boolean isDebugMode() {
		return debugMode;
	}

	public void setDebugMode(boolean debugMode) {
		this.debugMode = debugMode;
	}

	public boolean isBroMode() {
		return broMode;
	}

	public void setBroMode(boolean broMode) {
		this.broMode = broMode;
	}

	public Color getControlsColor() {
		return controlsColor;
	}

	public void setControlsColor(Color controlsColor) {
		this.controlsColor = controlsColor;
	}

	public Color getSmsMainColor() {
		return smsMainColor;
	}

	public void setSmsMainColor(Color smsMainColor) {
		this.smsMainColor = smsMainColor;
	}

	public Color getContactMainColor() {
		return contactMainColor;
	}

	public void setContactMainColor(Color contactMainColor) {
		this.contactMainColor = contactMainColor;
	}

	public Color getSettingsMainColor() {
		return settingsMainColor;
	}

	public void setSettingsMainColor(Color settingsMainColor) {
		this.settingsMainColor = settingsMainColor;
	}

	public boolean isDEBUGMODEDEFAULT() {
		return DEBUGMODEDEFAULT;
	}

	public boolean isBROMODEDEFAULT() {
		return BROMODEDEFAULT;
	}

	public Color getCONTROLSCOLORDEFAULT() {
		return CONTROLSCOLORDEFAULT;
	}

	public Color getSMSMAINCOLORDEFAULT() {
		return SMSMAINCOLORDEFAULT;
	}

	public Color getCONTACTMAINCOLORDEFAULT() {
		return CONTACTMAINCOLORDEFAULT;
	}

	public Color getSETTINGSMAINCOLORDEFAULT() {
		return SETTINGSMAINCOLORDEFAULT;
	}

	@Override
	public String toString() {
		return "Settings [phoneNumber=" + phoneNumber + ", debugMode="
				+ debugMode + ", DEBUGMODEDEFAULT=" + DEBUGMODEDEFAULT
				+ ", broMode=" + broMode + ", BROMODEDEFAULT=" + BROMODEDEFAULT
				+ ", controlsColor=" + controlsColor
				+ ", CONTROLSCOLORDEFAULT=" + CONTROLSCOLORDEFAULT
				+ ", smsMainColor=" + smsMainColor + ", SMSMAINCOLORDEFAULT="
				+ SMSMAINCOLORDEFAULT + ", contactMainColor="
				+ contactMainColor + ", CONTACTMAINCOLORDEFAULT="
				+ CONTACTMAINCOLORDEFAULT + ", settingsMainColor="
				+ settingsMainColor + ", SETTINGSMAINCOLORDEFAULT="
				+ SETTINGSMAINCOLORDEFAULT + "]";
	}
}
