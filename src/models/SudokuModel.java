/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone
 * Datum: 05.05.2015
 * Datei: SudokuModel.java
 */
package models;

import java.io.Serializable;
import java.util.Random;
import enums.DifficultyDescription;

/**
 * @author jor
 *
 */
public class SudokuModel implements Serializable {

	private static final long serialVersionUID = -1136135606029295553L;
	private static final int BOARD_WIDTH = 9;
	private static final int BOARD_HEIGHT = 9;
	private int[][] board;  //original sudoku
	private int[][] board2; //modifided sudoku
	private String saveTime;
	private DifficultyDescription diff;
	
	public SudokuModel() {
		this.board = new int[BOARD_WIDTH][BOARD_HEIGHT];
		this.board2 = new int[BOARD_WIDTH][BOARD_HEIGHT];
	}

	public int[][] nextBoard(DifficultyDescription difficulty)
	{
		this.board = new int[BOARD_WIDTH][BOARD_HEIGHT];
		this.diff = difficulty;
		nextCell(0,0);
		makeHoles(difficulty.getDifficulty());
		return board;
	}

	private boolean nextCell(int x, int y)
	{
		int nextX = x;
		int nextY = y;
		int[] toCheck = {1,2,3,4,5,6,7,8,9};
		Random r = new Random();
		int tmp = 0;
		int current = 0;
		int top = toCheck.length;

		for(int i=top-1;i>0;i--)
		{
			current = r.nextInt(i);
			tmp = toCheck[current];
			toCheck[current] = toCheck[i];
			toCheck[i] = tmp;
		}

		for(int i=0;i<toCheck.length;i++)
		{
			if(legalMove(x, y, toCheck[i]))
			{
				board[x][y] = toCheck[i];
				if(x == 8)
				{
					if(y == 8)
						return true;
					else
					{
						nextX = 0;
						nextY = y + 1;
					}
				}
				else
				{
					nextX = x + 1;
				}
				if(nextCell(nextX, nextY))
					return true;
			}
		}
		board[x][y] = 0;
		return false;
	}

	private boolean legalMove(int x, int y, int current) {
		for(int i=0;i<9;i++) {
			if(current == board[x][i])
				return false;
		}
		for(int i=0;i<9;i++) {
			if(current == board[i][y])
				return false;
		}
		int cornerX = 0;
		int cornerY = 0;
		if(x > 2)
			if(x > 5)
				cornerX = 6;
			else
				cornerX = 3;
		if(y > 2)
			if(y > 5)
				cornerY = 6;
			else
				cornerY = 3;
		for(int i=cornerX;i<10 && i<cornerX+3;i++)
			for(int j=cornerY;j<10 && j<cornerY+3;j++)
				if(current == board[i][j])
					return false;
		return true;
	}

	private void makeHoles(int holesToMake)
	{
		double remainingSquares = 81;
		double remainingHoles = (double)holesToMake;

		for(int i=0;i<9;i++)
			for(int j=0;j<9;j++)
			{
				double holeChance = remainingHoles/remainingSquares;
				if(Math.random() <= holeChance)
				{
					board[i][j] = 0;
					remainingHoles--;
				}
				remainingSquares--;
			}
	}
	
	public boolean isFinished(int [][] board){
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if(board[i][j] == 0) return false;
			}
		}
		return true;
	}
	
	public boolean isIdent(int [][] board, int [][] board2){
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if(board[i][j] != board2[i][j]) return false;
			}
		}
		return true;
	}
	
	public static int getBoardWidth() {
		return BOARD_WIDTH;
	}

	public static int getBoardHeight() {
		return BOARD_HEIGHT;
	}
	
	public int[][] getBoard() {
		return board;
	}

	public void setBoard(int[][] board) {
		this.board = board;
	}

	public String getSaveTime() {
		return saveTime;
	}

	public void setSaveTime(String saveTime) {
		this.saveTime = saveTime;
	}

	public DifficultyDescription getDiff() {
		return diff;
	}

	public void setDiff(DifficultyDescription diff) {
		this.diff = diff;
	}
	
	public int[][] getBoard2() {
		return board2;
	}

	public void setBoard2(int[][] board2) {
		this.board2 = board2;
	}

	public boolean solveSudoku(int [][] board){
		return SudokuSolver.solve(0, 0, board);
	}

	public int [][] copyBoard(int [][] board){
		int [][] board2 = new int[board.length][board[0].length]; 
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				board2[i][j]=board[i][j];
			}
		}
		return board2;
	}
	
	public void print()
	{
		for(int i=0;i<9;i++)
		{
			for(int j=0;j<9;j++)
				System.out.print(board[i][j] + "  ");
			System.out.println();
		}
	}
	public void print2()
	{
		for(int i=0;i<9;i++)
		{
			for(int j=0;j<9;j++)
				System.out.print(board2[i][j] + "  ");
			System.out.println();
		}
	}
}
