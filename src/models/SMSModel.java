/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 17.05.2015
 * Datei: SMSModel.java
 */
package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author jor
 *
 */
public class SMSModel implements Serializable{
	private static final long serialVersionUID = 3386273012857918860L;
	private List<SMS> messages;
	private List<Contact> conversation;

	public SMSModel() {
		super();
		this.messages = new ArrayList<SMS>();
		this.conversation = new ArrayList<Contact>();
	}

	public void sendSMS(SMS sms){
		messages.add(sms);
	}

	public void readSMS(String phoneNumber){
		for (SMS sms : messages) {
			if(!sms.isViewed() && sms.getFromNumber().compareToIgnoreCase(phoneNumber)==0)
				sms.setViewed(true);
		}
	}
	
	public int countAllUnreadSMS(){
		int count = 0;
		for(SMS s : messages) 
			if(!s.isViewed()) 
				count++;
		return count;
	}

	public int countUnreadSMSBetweenConversation(String senderNumber,String receiverNumber){
		int count = 0;
		for (SMS sms : messages) {
			if((sms.getFromNumber().compareToIgnoreCase(receiverNumber)== 0 && sms.getSendTo().getPhoneNumber().compareToIgnoreCase(senderNumber)==0 && !sms.isViewed()))
				count++;
		}
		return count;
	}

	public List<SMS> getSMS() {
		return messages;
	}

	public SMS [] getSMSList() {
		return messages.toArray(new SMS[messages.size()]);
	}

	public SMS [] getSMSListBetweenConversation(String senderNumber, String receiverNumber) {
		int count = 0;
		for (SMS sms : messages) {
			if((sms.getFromNumber().compareToIgnoreCase(senderNumber)== 0 && sms.getSendTo().getPhoneNumber().compareToIgnoreCase(receiverNumber)==0) ||(
					sms.getFromNumber().compareToIgnoreCase(receiverNumber)== 0 && sms.getSendTo().getPhoneNumber().compareToIgnoreCase(senderNumber)==0))
				count++;
		}
		if(count == 0) return null; 
		SMS [] sms = new SMS[count];
		int iterator = 0;
		for (SMS sms2 : messages) {
			if((sms2.getFromNumber().compareToIgnoreCase(senderNumber)== 0 && sms2.getSendTo().getPhoneNumber().compareToIgnoreCase(receiverNumber)==0) ||(
					sms2.getFromNumber().compareToIgnoreCase(receiverNumber)== 0 && sms2.getSendTo().getPhoneNumber().compareToIgnoreCase(senderNumber)==0)){
				sms[iterator] = sms2;
				iterator++;
			}			
		}
		return sms;
	}

	public List<Contact> getConversation() {
		return conversation;
	}

	public Contact [] getConversationList() {
		return conversation.toArray(new Contact[conversation.size()]);
	}

	public void addConversation(Contact c) {
		boolean adding = true;
		for(Contact e : conversation) {
			if(e.equals(c)) {
				adding = false;
				return;
			}
		}
		if(adding) {
			conversation.add(c);
			sortConversations();
		}
	}

	public void removeConversation(Contact c){
		int index = -1;
		int counter = 0;
		for(Contact e : conversation) {
			if(e.equals(c)) {
				index = counter;
			}
			counter++;
		}
		conversation.remove(index);
		sortConversations();
	} 

	private void sortConversations(){
		Collections.sort(conversation, new Comparator<Contact>() {
			@Override
			public int compare(Contact o1, Contact o2) {
				return o1.getLname().compareTo(o2.getLname());
			}
		});
	}

	public Contact [] searchConversations(String s){
		List<Contact> result = conversation.stream().filter((Contact c) -> (c.getLname().concat(" ").concat(c.getFname())).toLowerCase().startsWith(s.toLowerCase()) || c.getFname().toLowerCase().startsWith(s.toLowerCase())).collect(Collectors.toList());
		return result.toArray(new Contact[result.size()]);
	}
}
