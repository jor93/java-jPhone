/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 27.04.2015
 * Datei: Contact.java
 */
package models;

import java.io.Serializable;

/**
 * @author jor
 *
 */
public class Contact implements Serializable{
	private static final long serialVersionUID = 4813692547981058802L;
	private String fname;
	private String lname;
	private String phoneNumber;
	private String email;

	public Contact() {
		super();
	}

	public Contact(String fname, String lname, String phoneNumber) {
		this(fname,lname,phoneNumber,"");
	}

	public Contact(String fname, String lname, String phoneNumber, String email) {
		super();
		this.fname = fname;
		this.lname = lname;
		this.phoneNumber = phoneNumber;
		this.email = email;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean equals(Contact c) {
		return this.getFname().equals(c.getFname())
				&& this.getLname().equals(c.getLname())
				&& this.getPhoneNumber().equals(c.getPhoneNumber())
				&& this.getEmail().equals(c.getEmail());
	}
	
	public boolean equalsPhoneNumber(String phoneNumber) {
		return this.getPhoneNumber().equals(phoneNumber);
	}

	@Override
	public String toString() {
		return "Contact [first name=" + fname + ", last name=" + lname
				+ ", phoneNumber=" + phoneNumber + ", email=" + email + "]";
	}		
}
