/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 17.05.2015
 * Datei: SMS.java
 */
package models;

import java.io.Serializable;
import java.util.Date;

/**
 * @author jor
 *
 */
public class SMS implements Serializable{
	private static final long serialVersionUID = 444454812841764171L;
	private String fromNumber;
	private Contact sendTo;
	private String text;
	private Date date;
	private boolean viewed = false;
	
	public SMS() {
		super();
	}

	public SMS(String fromNumber, Contact sendTo, String text, Date date) {
		super();
		this.fromNumber = fromNumber;
		this.sendTo = sendTo;
		this.text = text;
		this.date = date;
	}

	public String getFromNumber() {
		return fromNumber;
	}

	public void setFromNumber(String fromNumber) {
		this.fromNumber = fromNumber;
	}

	public Contact getSendTo() {
		return sendTo;
	}

	public void setSendTo(Contact sendTo) {
		this.sendTo = sendTo;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isViewed() {
		return viewed;
	}

	public void setViewed(boolean viewed) {
		this.viewed = viewed;
	}

	@Override
	public String toString() {
		return "SMS [from=" + fromNumber + ", to=" + sendTo
				+ ", message=" + text + ", date=" + date + ", viewed=" + viewed
				+ "]";
	}
	
}
