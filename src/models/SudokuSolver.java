/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 05.05.2015
 * Datei: SudokuSolver.java
 */
package models;

/**
 * @author jor
 *
 */
public class SudokuSolver {

	private static final int UNASSIGNED = 0; 

	public static boolean solve(int row, int col, int[][] cells) {  
		if (row == 9) {  
			row = 0;  
			if (++col == 9)  
				return true;  
		}  
		if (cells[row][col] != UNASSIGNED)
			return solve(row + 1, col, cells);  
		for (int val = 1; val <= 9; ++val) {  
			if (isSafe(row, col, val, cells)) {  
				cells[row][col] = (byte) val;  
				if (solve(row + 1, col, cells))  
					return true;  
			}  
		}  
		cells[row][col] = UNASSIGNED;
		return false;  
	}  

	private static boolean isSafe(int row, int col, int val, int[][] cells) {  
		for (int k = 0; k < 9; ++k)  
			if (val == cells[k][col])  
				return false;  
		for (int k = 0; k < 9; ++k)  
			if (val == cells[row][k])  
				return false;  
		int boxRowOffset = (row / 3) * 3;  
		int boxColOffset = (col / 3) * 3;  
		for (int k = 0; k < 3; ++k)  
			for (int m = 0; m < 3; ++m)  
				if (val == cells[boxRowOffset + k][boxColOffset + m])  
					return false;  
		return true;  
	}  

}
