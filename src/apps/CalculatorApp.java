/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 29.04.2015
 * Datei: CalculatorApp.java
 */
package apps;

import enums.AppsDescription;
import heredity.Apps;

/**
 * @author jor
 *
 */
public class CalculatorApp extends Apps{
	private static final long serialVersionUID = -6408504352866481990L;

	public CalculatorApp(String appName, AppsDescription ad, float version, String phoneNumber) {
		super(appName,ad,version,phoneNumber);
	}


}
