/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 24.05.2015
 * Datei: SettingsApp.java
 */
package apps;

import enums.AppsDescription;
import heredity.Apps;

/**
 * @author jor
 *
 */
public class SettingsApp extends Apps {
	private static final long serialVersionUID = 8359221625761808488L;

	public SettingsApp(String appName, AppsDescription ad, float version,
			String phoneNumber) {
		super(appName, ad, version, phoneNumber);
	}

}
