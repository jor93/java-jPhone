/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 29.04.2015
 * Datei: SMSApp.java
 */
package apps;

import enums.AppsDescription;
import heredity.Apps;

/**
 * @author jor
 *
 */
public class SMSApp extends Apps{
	private static final long serialVersionUID = -2125172236935610110L;

	public SMSApp(String appName, AppsDescription ad,float version,String phoneNumber) {
		super(appName,ad,version,phoneNumber);
	}

}
