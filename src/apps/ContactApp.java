/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 29.04.2015
 * Datei: ContactApp.java
 */
package apps;

import enums.AppsDescription;
import heredity.Apps;

/**
 * @author jor
 *
 */
public class ContactApp extends Apps{
	private static final long serialVersionUID = 5401474689611166961L;

	public ContactApp(String appName, AppsDescription ad,float version,String phoneNumber) {
		super(appName,ad,version,phoneNumber);
	}
}
