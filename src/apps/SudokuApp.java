/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 29.04.2015
 * Datei: SudokuApp.java
 */
package apps;

import enums.AppsDescription;
import heredity.Apps;

/**
 * @author jor
 *
 */
public class SudokuApp extends Apps{
	private static final long serialVersionUID = 3858688290548741526L;

	public SudokuApp(String appName, AppsDescription ad,float version,String phoneNumber) {
		super(appName,ad,version,phoneNumber);
	}
	
	
}
