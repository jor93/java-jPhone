/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 30.04.2015
 * Datei: JPhone.java
 */
package JPhone;

import heredity.Apps;
import heredity.Controls;
import heredity.Form;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import settings.GlobalSettings;
import views.HomeScreen;
import views.LockScreen;
import views.SudokuViewGame;
import views.SudokuViewMain;
import apps.CalculatorApp;
import apps.ContactApp;
import apps.SMSApp;
import apps.SettingsApp;
import apps.SudokuApp;
import controllers.BackgroundController;
import controllers.ContactController;
import controllers.SMSController;
import controllers.SettingsController;
import controllers.SudokuController;
import enums.AppsDescription;
import enums.AppsName;

/**
 * @author jor
 *
 */
public class JPhone extends JFrame implements Serializable{
	private static final long serialVersionUID = -6447955222344829732L;
	private String phoneNumber;
	private ArrayList<JPanel> views;
	private ContactController contactController;
	private SudokuController sudokuController;
	private BackgroundController bgController;
	private SMSController smsController;
	private SettingsController settingsController;
	public ArrayList<Apps> appList;
	private Controls controls;
	private int messages;
	private int bgNumber;

	public JPhone(String phoneNumber){
		super(phoneNumber);
		this.setLayout(null);
		this.phoneNumber = phoneNumber;
		this.views = new ArrayList<>();
		
		this.settingsController = new SettingsController(phoneNumber);
		this.contactController = new ContactController(phoneNumber);
		this.sudokuController = new SudokuController(phoneNumber);
		this.smsController = new SMSController(phoneNumber);

		int zufall = (int)(Math.random()*4)+1;
		this.bgController = new BackgroundController(phoneNumber,zufall);
		this.bgNumber = getBgController().getNumber();

		this.setSize(GlobalSettings.width, GlobalSettings.height);
		this.setResizable(false);
		this.addWindowListener(new Close());
		this.setVisible(true);

		messages = smsController.countAllUnreadSMS();

		this.appList = new ArrayList<>();
		addAppstoList(new ContactApp(AppsName.CONTACT.getName(), AppsDescription.BASIC,1.0f,phoneNumber));
		addAppstoList(new SMSApp(AppsName.SMS.getName(), AppsDescription.BASIC,1.0f,phoneNumber));
		addAppstoList(new CalculatorApp(AppsName.CALCULATOR.getName(), AppsDescription.UTILITY,1.0f,phoneNumber));
		addAppstoList(new SudokuApp(AppsName.SUDOKU.getName(), AppsDescription.GAME,1.0f,phoneNumber));	
		addAppstoList(new SettingsApp(AppsName.SETTINGS.getName(), AppsDescription.BASIC, 1.0f, phoneNumber));

		controls = new Controls(false) {
			private static final long serialVersionUID = -5530887057701628157L;
			@Override
			public void lock(){ 
				if((views.get(views.size()-1)) instanceof SudokuViewGame){
					((SudokuViewGame)(views.get(views.size()-1))).saveTime();
					sudokuController.save();
				}
				lockPanel(); 
				if(getSettingsController().getSettings().isDebugMode())
					System.out.println("jPhone " + phoneNumber + " locked");
			}

			@Override
			public void home(){ 
				if(!(getContentPane() instanceof HomeScreen)){
					if((views.get(views.size()-1)) instanceof SudokuViewGame){
						((SudokuViewGame)(views.get(views.size()-1))).saveTime();
						sudokuController.save();
					}
					homePanel();
					removeAllPanel();
					if(getSettingsController().getSettings().isDebugMode())
						System.out.println("jPhone " + phoneNumber + " gone home");
				}
			}

			@Override
			public void back(){ 
				removeLastPanel(); 
				if(getSettingsController().getSettings().isDebugMode())
					System.out.println("jPhone " + phoneNumber + " back");
			}
		};
		controls.setLocation(0, GlobalSettings.height-GlobalSettings.heigthControl);
		add(controls);
	}

	private class Close extends WindowAdapter implements Serializable{
		private static final long serialVersionUID = 2382858126107304061L;

		@Override
		public void windowClosing(WindowEvent e) {
			if((views.get(views.size()-1)) instanceof SudokuViewGame){
				((SudokuViewGame)(views.get(views.size()-1))).saveTime();
				sudokuController.save();
			}
			removeAllPanel();
			repaint();	
			setVisible(false);
			getSMSController().readSMS(phoneNumber);
			setMessages(getSMSController().countAllUnreadSMS());
			lockPanel();
			setBgNumber(getBgController().getNumber());
			if(getSettingsController().getSettings().isDebugMode())
				System.out.println("jPhone " + phoneNumber +" closed");
			close();
			Init.jPhoneController.save();
		}
	}

	public void close(){
		bgController.save();
		settingsController.save();
		bgController = null;
		sudokuController = null;
		settingsController = null;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public Apps [] getAppList() {
		return appList.toArray(new Apps[appList.size()]);
	}

	public void addAppstoList(Apps app){
		boolean adding = true;
		for(Apps a2 : appList) {
			if(a2.equals(app)) {
				adding = false;
				return;
			}
		}
		if(adding) {
			appList.add(app);
		}
	}	

	public int findApp(String appName){
		for (int i = 0; i < appList.size(); i++) {
			if(appList.get(i).getAppName().compareTo(appName) == 0)
				return i;
		}
		return -1;
	}
	
	public boolean appInList(String appName){
		for (int i = 0; i < appList.size(); i++) {
			if(appList.get(i).getAppName().compareTo(appName) == 0)
				return true;
		}
		return false;
	}

	public JPanel getViews(int index) {
		return views.get(index);
	}

	public int getViewsSize() {
		return views.size();
	}
	
	public ContactController getContactController() {
		return contactController;
	}

	public void setContactController(ContactController contactController) {
		this.contactController = contactController;
	}

	public SudokuController getSudokuController() {
		return sudokuController;
	}

	public void setSudokuController(SudokuController sudokuController) {
		this.sudokuController = sudokuController;
	}

	public BackgroundController getBgController() {
		return bgController;
	}

	public void setBgController(BackgroundController bgController) {
		this.bgController = bgController;
	}

	public SMSController getSMSController() {
		return smsController;
	}

	public void setSMSController(SMSController smsController) {
		this.smsController = smsController;
	}

	public SettingsController getSettingsController() {
		return settingsController;
	}

	public void setSettingsController(SettingsController settingsController) {
		this.settingsController = settingsController;
	}

	public int getBgNumber() {
		return bgNumber;
	}

	public void setBgNumber(int bgNumber) {
		this.bgNumber = bgNumber;
	}

	public Controls getControls() {
		return controls;
	}

	public int getMessages() {
		return messages;
	}

	public void updateBG(Icon bg){
		getHomeScreen().setBg(bg);
		getLockScreen().setBg(bg);
	}
	
	public void setMessages(int messages) {
		this.messages = messages;
		if(messages == 0){
			getLockScreen().showNewMessage(false);
			getLockScreen().repaint();
			return;
		}
		getLockScreen().showNewMessage(true);
		getLockScreen().repaint();
	}

	public void getLatestPanel(){
		if(views.size() > 1){
			if(views.get(views.size()-1) instanceof SudokuViewGame){
				int index = findApp(AppsName.SUDOKU.getName());
				addPanel(((SudokuApp)(appList.get(index))).getView(1));
				JPanel p = ((SudokuApp)(appList.get(index))).getView(2);
				getSudokuController().read();
				((SudokuViewGame)(p)).startTimer(getSudokuController().getsModel().getSaveTime());
			}
			this.setContentPane(views.get(views.size()-1));
			this.add(controls);
			this.controls.setShowMode(true);
			this.controls.repaint();
			this.repaint();
		}
	}

	public void addPanel(JPanel p) {
		boolean adding = true;
		for(JPanel temp : views) {
			if(temp.equals(p)) {
				adding = false;
				return;
			}
		}
		if(adding) {
			views.add(p);
			if(views.get(views.size()-1) instanceof SudokuViewMain)
				((SudokuViewMain)appList.get(3).getView(0)).checkSavegame();	
			this.setContentPane(p);		
			if(views.size() > 1){
				this.add(controls);
				this.controls.setShowMode(true);	
				this.controls.repaint();
				this.repaint();
			}
		}
	}

	public JPanel getPanel(int index) {
		return views.get(index);
	}

	public JPanel getCurrentPanel() {
		return views.get(views.size()-1);
	}

	public void removeLastPanel(){
		if(views.get(views.size()-1) instanceof SudokuViewGame){
			((SudokuViewGame)(views.get(views.size()-1))).saveTime();
			sudokuController.save();
		}
		if(views.get(views.size()-1) instanceof Form){
			((Form)(views.get(views.size()-1))).resetText();
		}
		if(views.size() > 2)
			views.remove(views.size()-1);
		if(views.get(views.size()-1) instanceof SudokuViewMain)
			((SudokuViewMain)appList.get(3).getView(0)).checkSavegame();	
		this.setContentPane(views.get(views.size()-1));
		this.add(controls);
		this.controls.setShowMode(true);
		this.controls.repaint();
		this.repaint();
	}

	private void removeAllPanel(){
		while(views.size() > 2){
			if(views.get(views.size()-1) instanceof Form){
				((Form)(views.get(views.size()-1))).resetText();
			}
			views.remove(views.size()-1);
		}
		this.add(controls);
		this.controls.setShowMode(true);
		this.controls.repaint();
		this.repaint();
	}

	public LockScreen getLockScreen(){
		return (LockScreen)views.get(0);
	}

	private void lockPanel(){
		this.setContentPane(views.get(0));
		this.controls.setShowMode(false);
		this.controls.repaint();
		this.repaint();	
	}

	public HomeScreen getHomeScreen(){
		return (HomeScreen)views.get(1);
	}

	private void homePanel(){
		this.setContentPane(views.get(1));
	}
}
