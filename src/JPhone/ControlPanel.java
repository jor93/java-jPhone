/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 28.04.2015
 * Datei: ControlPanel.java
 */
package JPhone;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import settings.GlobalSettings;
import JPhone.Init;

/**
 * @author jor
 *
 */
public abstract class ControlPanel extends JFrame {
	private static final long serialVersionUID = 4108715540850833228L;
	private JLabel labelAddPhone;
	private JTextField textAddPhone;
	private JButton buttonAdd;
	private JLabel labelSelectPhone;
	protected JComboBox<String> comboSelectPhone;
	private JButton buttonStart;
	private JButton buttonDelete;
	private EmptyBorder empty1; // Text
	private EmptyBorder empty2; // Buttons,TextField,Combo
	private JLabel info;

	private File config;
	private JPanel panel;

	protected String number;
	protected int counter = 0;

	public ControlPanel(String s){
		super(s);

		config = new File("./config");
		if(config.exists() == false) {
			config.mkdir();
		}

		this.setResizable(false);
		this.setVisible(true);
		this.setLocation(50,50);
		this.addWindowListener(new Close());
		this.setLayout(null);

		Listeners l = new Listeners();

		empty1 = new EmptyBorder(0, 20, 0, 0);
		empty2 = new EmptyBorder(20, 20, 0, 20);

		panel = new JPanel();
		panel.setBounds(0, 0, GlobalSettings.widthPanel, GlobalSettings.heightPanel-30);
		panel.setLayout(new GridLayout(4, 2, 20,20));
		panel.setBorder(empty2);

		labelAddPhone = new JLabel("Add jPhone:");
		labelAddPhone.setBorder(empty1);
		labelAddPhone.setFont(GlobalSettings.globalFont(18));
		panel.add(labelAddPhone);

		textAddPhone = new JTextField(30);
		textAddPhone.setFont(GlobalSettings.globalFont(18));
		KeyListeners k = new KeyListeners();
		textAddPhone.addKeyListener(k);
		panel.add(textAddPhone);
		panel.add(new JLabel());

		buttonAdd = new JButton("Add");
		buttonAdd.addActionListener(l);
		buttonAdd.setFocusPainted(false);
		buttonAdd.setFont(GlobalSettings.globalFont(18));
		panel.add(buttonAdd);

		labelSelectPhone = new JLabel("Select jPhone:");
		labelSelectPhone.setBorder(empty1);
		labelSelectPhone.setFont(GlobalSettings.globalFont(18));
		panel.add(labelSelectPhone);

		comboSelectPhone = new JComboBox<>(getPhonesFromDir(config));
		comboSelectPhone.setEditable(false);
		panel.add(comboSelectPhone);

		buttonStart = new JButton("Start jPhone");
		buttonStart.setFocusPainted(false);
		buttonStart.addActionListener(l);
		buttonStart.setFont(GlobalSettings.globalFont(18));
		panel.add(buttonStart);

		buttonDelete = new JButton("Delete jPhone");
		buttonDelete.setFont(GlobalSettings.globalFont(18));
		buttonDelete.setFocusable(false);
		buttonDelete.addActionListener(l);
		panel.add(buttonDelete);		
		add(panel);

		info = new JLabel("Made with \u2665 by jor");
		info.setBounds(340, GlobalSettings.heightPanel-25, 100, 15);
		info.setFont(GlobalSettings.globalFont(10));
		info.setForeground(Color.GRAY);
		add(info);

		textAddPhone.requestFocus();
	}

	public class KeyListeners extends KeyAdapter{
		@Override
		public void keyTyped(KeyEvent e) {
			char c = e.getKeyChar();
			if(!(Character.isDigit(c) || c== KeyEvent.VK_BACK_SPACE)){
				e.consume();
				return;
			}
			if(c== KeyEvent.VK_BACK_SPACE){
				counter--;
				if(counter < 0){
					counter = 0;
					e.consume();
				}		
				return;
			}			
			if(counter > 12) {e.consume();return;}
			if(counter == 3 || counter == 7 || counter == 10){
				textAddPhone.setText(textAddPhone.getText()+" ");
				counter += 2;
			}else
				counter++;
		}
	}

	private class Listeners implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			switch (e.getActionCommand()) {
			case "Add":
				if(textAddPhone.getText().length() < 1) return;
				if(textAddPhone.getText().length() < 13) {
					JOptionPane.showMessageDialog(panel,"Phone Number Format Error","Error",JOptionPane.ERROR_MESSAGE); 
					counter = textAddPhone.getText().length();
					return;
				}
				if(textAddPhone.getText().length() == 13 && textAddPhone.getText().compareTo("") != 0){
					File phone = new File("./config/"+textAddPhone.getText());
					if(!phone.exists()) {
						phone.mkdir();
						comboSelectPhone.setModel(new DefaultComboBoxModel<String>(getPhonesFromDir(config)));
					}
					else
						JOptionPane.showMessageDialog(panel,"jPhone " + textAddPhone.getText() + " already exists!","Error",JOptionPane.ERROR_MESSAGE);
					textAddPhone.setText("");
					counter = 0;
				}
				break;
			case "Start jPhone":
				String phoneNumber = (String)comboSelectPhone.getSelectedItem();
				if(phoneNumber != null)
					startJPhone(phoneNumber);
				else
					JOptionPane.showMessageDialog(panel,"No jPhone selected!","Error",JOptionPane.ERROR_MESSAGE);
				break;
			case "Delete jPhone":
				if(comboSelectPhone.getItemCount() == 0) return; 
				if(Init.Panel.phoneList.size() != 0 ){
					if(Init.Panel.findJPhone((String)comboSelectPhone.getSelectedItem()) != null){
						if(Init.Panel.findJPhone((String)comboSelectPhone.getSelectedItem()).isVisible()){
							JOptionPane.showMessageDialog(panel,"jPhone "+ (String)comboSelectPhone.getSelectedItem() +" is still running!","Error",JOptionPane.ERROR_MESSAGE);
							return;
						}
						if(Init.Panel.findInList((String)comboSelectPhone.getSelectedItem())){
							int index = Init.Panel.findJPhoneByNumberInList(((String)comboSelectPhone.getSelectedItem()));
							if(index != -1){
								Init.Panel.phoneList.remove(index);
								Init.jPhoneController.save();
							}
						}
					}
				}
				File f = new File("./config/"+(String)comboSelectPhone.getSelectedItem());
				File [] files = f.listFiles();
				if(files != null){
					for (int i = 0; i < files.length; i++) {
						files[i].setWritable(true);
						files[i].delete();
						System.gc();
					}
				}
				if(GlobalSettings.debugMode)
					System.out.println("jPhone "+ f.getName() +" deleted");
				f.delete();
				File fi = new File("./config/");
				File [] fis = fi.listFiles();
				if(fis.length == 1){
					fis[0].delete();
					System.gc();
				}
				comboSelectPhone.removeItemAt(comboSelectPhone.getSelectedIndex());
				break;
			}
		}
	}

	private class Close extends WindowAdapter{
		@Override
		public void windowClosing(WindowEvent e) {
			boolean closing = true;
			for (JPhone j : Init.Panel.phoneList){
				if(j.isVisible()){
					closing = false;
					break;
				}
			}
			if(closing){
				if(Init.Panel.phoneList.size() > 0){
					Init.jPhoneController.save();
				}else{
					File f = new File("./config/phones.ser");
					f.delete();
				}
				System.exit(0);
			} else{
				JOptionPane.showMessageDialog(panel,"Some jPhone are still open","Error",JOptionPane.ERROR_MESSAGE); 
				setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
			}
		}
	}

	private String [] getPhonesFromDir(File f){
		int counter = 0;
		File [] temp = f.listFiles();
		for (int i = 0; i < temp.length; i++) {
			if(temp[i].isDirectory())
				counter++;
		}
		String [] array = new String[counter];
		for (int i = 0; i < array.length; i++) {
			array[i] = temp[i].getName();
		}
		return array;

	}

	public abstract void startJPhone(String phoneNumber);
}
