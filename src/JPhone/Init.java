/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 28.04.2015
 * Datei: Init.java
 */
package JPhone;

import java.util.ArrayList;
import controllers.BackgroundController;
import controllers.JPhoneController;
import controllers.SettingsController;
import controllers.SudokuController;
import settings.GlobalSettings;
import views.LockScreen;

/**
 * @author jor
 *
 */
public class Init{

	public static JPhoneController jPhoneController;

	public static class Panel extends ControlPanel{
		private static final long serialVersionUID = 900343173008667808L;
		public static ArrayList<JPhone> phoneList;

		public Panel(String s){
			super(s);	
			jPhoneController = new JPhoneController();			
		}
		public static void loadPhones(){
			if(!jPhoneController.isFirstBoot()){
				jPhoneController.read();
			}
		}
		
		public static void setPhoneList(ArrayList<JPhone> phoneList) {
			Panel.phoneList = phoneList;
		}

		public static ArrayList<JPhone> getPhoneList() {
			return phoneList;
		}

		public static int findJPhoneByNumberInList(String phoneNumber){
			for (int i = 0; i < phoneList.size(); i++) {
				if(phoneList.get(i).getPhoneNumber().equals(phoneNumber)) 
					return i;
			}
			return -1;
		}

		public static boolean findInList(String phoneNumber){
			for (JPhone j : phoneList) {
				if(j.getPhoneNumber().equals(phoneNumber)) 
					return true;
			}
			return false;
		}

		public static JPhone findJPhone(String phoneNumber){
			for (JPhone j : phoneList) {
				if(j.getPhoneNumber().equals(phoneNumber)) 
					return j;
			}
			return null;
		}

		@Override
		public void startJPhone(String phoneNumber) {
			boolean first = true;
			for (JPhone j : phoneList) {
				if(j.getPhoneNumber().equals(phoneNumber)) {
					if(j.isVisible()){
						if(GlobalSettings.debugMode)
							System.out.println("jPhone " + phoneNumber + " is already started");
						return;
					}
					first = false;
					j.setSettingsController(new SettingsController(j.getPhoneNumber()));
					j.setSudokuController(new SudokuController(j.getPhoneNumber()));
					j.setBgController(new BackgroundController(j.getPhoneNumber()));
					j.setBgNumber(j.getBgController().getNumber());
					((LockScreen)j.getViews(0)).startTimer();
					j.setMessages(j.getMessages());
					j.setVisible(true);
					if(j.getSettingsController().getSettings().isDebugMode())
						System.out.println("jPhone " + phoneNumber + " started");
					break;
				}
			}
			if(first) {
				JPhone jPhone = new JPhone(phoneNumber);
				jPhone.setLocation(550, 50);
				phoneList.add(jPhone);
				jPhone.setVisible(true);
				jPhone.addPanel(new LockScreen(phoneNumber));
				jPhone.setMessages(jPhone.getMessages());
				jPhone.setContentPane(jPhone.getViews(0));
				if(jPhone.getSettingsController().getSettings().isDebugMode())
					System.out.println("jPhone " + phoneNumber + " sucessfully created");
			}
		}
	}

	public static void main(String [] args){
		Panel control = new Init.Panel("Control Panel");
		control.setSize(GlobalSettings.widthPanel+10,GlobalSettings.heightPanel+30);
		Init.Panel.loadPhones();
	}

}

