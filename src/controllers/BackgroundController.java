/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 01.05.2015
 * Datei: BackgroundController.java
 */
package controllers;

import heredity.PanelManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import JPhone.Init;
import JPhone.JPhone;
import settings.GlobalSettings;

/**
 * @author jor
 *
 */
public class BackgroundController implements PanelManager,Serializable {
	private static final long serialVersionUID = -480661545225450861L;
	private Icon bg;
	private String phoneNumber;
	private int number;

	public BackgroundController(String phoneNumber) {
		super();
		this.phoneNumber = phoneNumber;
		File f = new File("./config/"+this.phoneNumber+"/bg.ser");
		if(f.exists() && !f.isDirectory())
			read();
		this.number = getPhone().getBgNumber();
	}		

	public BackgroundController(String phoneNumber,int zufall) {
		super();
		this.phoneNumber = phoneNumber;
		switch (zufall) {
		case 1:
			this.bg = new ImageIcon(GlobalSettings.backgroundImage1);
			this.number = 1;
			break;
		case 2:
			this.bg = new ImageIcon(GlobalSettings.backgroundImage2);
			this.number = 2;
			break;
		case 3:
			this.bg = new ImageIcon(GlobalSettings.backgroundImage3);
			this.number = 3;
			break;
		case 4:
			this.bg = new ImageIcon(GlobalSettings.backgroundImage4);
			this.number = 4;
			break;
		}
		save();
	}

	public void read(){
		Object ser = new Object();
		try {
			FileInputStream fis = new FileInputStream("./config/"+this.phoneNumber+"/bg.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			ser = ois.readObject();
			ois.close();
			fis.close();
		} catch (Exception e) { }
		if(ser instanceof Icon)
			bg = (Icon) ser;
	}

	public void save(){
		try {
			FileOutputStream fos = new FileOutputStream("./config/"+this.phoneNumber+"/bg.ser");
			ObjectOutputStream oos = new ObjectOutputStream (fos);
			oos.writeObject(bg);
			oos.close();
			fos.close();
		} catch (Exception e) {}
	}
	
	public void updateBG(Icon bg){
		this.bg = bg;
		getPhone().updateBG(bg);
		if(getPhone().getSettingsController().getSettings().isDebugMode()){
			System.out.println("jPhone " + phoneNumber + " Background changed");
		}
		save();
	}
	
	public Icon getBg() {
		return bg;
	}	
	
	public int getNumber(){
		return number;
	}

	@Override
	public void addPanel(JPanel p) {
		getPhone().addPanel(p);
	}

	@Override
	public void removePanel(JPanel p) {
	}

	@Override
	public JPhone getPhone() {
		return Init.Panel.findJPhone(phoneNumber);
	}
}

