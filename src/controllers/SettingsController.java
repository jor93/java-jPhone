/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 25.05.2015
 * Datei: SettingsController.java
 */
package controllers;

import heredity.PanelManager;
import heredity.SampleView;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import enums.AppsName;
import JPhone.Init;
import JPhone.JPhone;
import settings.GlobalSettings;
import models.Settings;

/**
 * @author jor
 *
 */
public class SettingsController implements PanelManager, Serializable{
	private static final long serialVersionUID = -4068390466129536470L;
	private String phoneNumber;
	private Settings settings;

	public SettingsController(String phoneNumber) {
		super();
		this.phoneNumber = phoneNumber;
		File f = new File("./config/"+this.phoneNumber+"/settings.ser");
		if(f.exists() && !f.isDirectory())
			read();
		else
			this.settings = new Settings();
	}

	public void read(){
		Object ser = new Object();
		try {
			FileInputStream fis = new FileInputStream("./config/"+this.phoneNumber+"/settings.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			ser = ois.readObject();
			ois.close();
			fis.close();
		} catch (Exception e) {}
		if(ser instanceof Settings)
			settings = (Settings) ser;
	}

	public void save(){
		try {
			FileOutputStream fos = new FileOutputStream("./config/"+this.phoneNumber+"/settings.ser");
			ObjectOutputStream oos = new ObjectOutputStream (fos);
			oos.writeObject(settings);
			oos.close();
			fos.close();
		} catch (Exception e) {}
	}

	public Settings getSettings() {
		return settings;
	}

	public void activateBroMode(){
		if(getSettings().isBroMode()){
			getPhone().getControls().setIconToHome(new ImageIcon(GlobalSettings.brofist));
		}else{
			getPhone().getControls().setIconToHome(new ImageIcon(GlobalSettings.home));
		}		
	}
	
	public void setColorControls(Color c){
		getPhone().getControls().setBackground(c);
		getPhone().getControls().repaint();
		if(getPhone().getSettingsController().getSettings().isDebugMode())
			System.out.println("jPhone " + getPhone().getPhoneNumber() +" Controls Color changed");
	}

	public void setColorSMS(Color c){
		JPanel [] views;
		getSettings().setSmsMainColor(c);
		if(getPhone().getSettingsController().getSettings().isDebugMode())
			System.out.println("jPhone " + getPhone().getPhoneNumber() +" SMS Color changed");
		int index = getPhone().findApp(AppsName.SMS.getName());
		if(index != -1){
			views = getPhone().appList.get(index).getViewsArray();
			for (int i = 0; i < views.length; i++) {
				((SampleView)views[i]).setColor(c);
			}
		}
	}

	public void setColorContact(Color c){
		JPanel [] views;
		getSettings().setContactMainColor(c);
		if(getPhone().getSettingsController().getSettings().isDebugMode())
			System.out.println("jPhone " + getPhone().getPhoneNumber() +" Contact Color changed");
		int index = getPhone().findApp(AppsName.CONTACT.getName());
		if(index != -1){
			views = getPhone().appList.get(index).getViewsArray();
			for (int i = 0; i < views.length; i++) {
				((SampleView)views[i]).setColor(c);
			}
		}
	}

	public void setColorSettings(Color c){
		JPanel [] views;
		getSettings().setSettingsMainColor(c);
		if(getPhone().getSettingsController().getSettings().isDebugMode())
			System.out.println("jPhone " + getPhone().getPhoneNumber() +" Setting Color changed");
		int index = getPhone().findApp(AppsName.SETTINGS.getName());
		if(index != -1){
			views = getPhone().appList.get(index).getViewsArray();
			for (int i = 0; i < views.length; i++) {
				((SampleView)views[i]).setColor(c);
				views[i].repaint();
			}
		}
	}

	@Override
	public void addPanel(JPanel p) {
		getPhone().addPanel(p);	
	}

	@Override
	public void removePanel(JPanel p) {}

	@Override
	public JPhone getPhone() {
		return Init.Panel.findJPhone(this.phoneNumber);
	}
}
