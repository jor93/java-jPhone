/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 29.04.2015
 * Datei: ContactController.java
 */
package controllers;

import heredity.PanelManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.List;
import javax.swing.JPanel;
import JPhone.Init;
import JPhone.JPhone;
import models.Contact;
import models.ContactModel;

/**
 * @author jor
 *
 */
public class ContactController implements PanelManager, Serializable{
	private static final long serialVersionUID = 3073549150503167872L;
	private ContactModel contactModel;
	private String phoneNumber;

	public ContactController(String phoneNumber) {
		super();
		this.phoneNumber = phoneNumber;
		File f = new File("./config/"+this.phoneNumber+"/contacts.ser");
		if(f.exists() && !f.isDirectory())
			read();
		else
			this.contactModel = new ContactModel();
	}

	public void read(){
		Object ser = new Object();
		try {
			FileInputStream fis = new FileInputStream("./config/"+this.phoneNumber+"/contacts.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			ser = ois.readObject();
			ois.close();
			fis.close();
		} catch (Exception e) {}
		if(ser instanceof ContactModel)
			contactModel = (ContactModel) ser;
	}

	public void save(){
		try {
			FileOutputStream fos = new FileOutputStream("./config/"+this.phoneNumber+"/contacts.ser");
			ObjectOutputStream oos = new ObjectOutputStream (fos);
			oos.writeObject(contactModel);
			oos.close();
			fos.close();
		} catch (Exception e) {}
	}

	public Contact[] getContactList() {
		return contactModel.getContactList();
	}

	public List<Contact> getContacts(){
		return contactModel.getContacts();
	}
	
	public ContactModel getContactModel() {
		return contactModel;
	}

	public Contact getContactByPhone(String phoneNumber) {
		return contactModel.getContactByPhone(phoneNumber);
	}

	public boolean alreadyInList(String phoneNumber){
		return contactModel.alreadyInList(phoneNumber);
	}
	
	public void addContact(Contact c) {
		if(getPhone().getSettingsController().getSettings().isDebugMode())
			System.out.println("jPhone " + this.phoneNumber + " " + c.toString() + " added");
		contactModel.addContact(c);
		save();
	}

	public void removeContact(Contact c) {
		if(getPhone().getSettingsController().getSettings().isDebugMode())
			System.out.println("jPhone " + this.phoneNumber + " " + c.toString() + " deleted");
		contactModel.removeContact(c);
		save();
	} 

	public void updateContact(Contact old, Contact c) {
		if(getPhone().getSettingsController().getSettings().isDebugMode())
			System.out.println("jPhone " + this.phoneNumber + " " + c.toString() + " updated");
		contactModel.updateContact(old, c);
		save();
	}

	@Override
	public void addPanel(JPanel p) {
		getPhone().addPanel(p);	
	}

	@Override
	public void removePanel(JPanel p) {}

	@Override
	public JPhone getPhone() {
		return Init.Panel.findJPhone(this.phoneNumber);
	}
}
