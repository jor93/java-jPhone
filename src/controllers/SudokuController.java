/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 05.05.2015
 * Datei: SudokuController.java
 */
package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import enums.DifficultyDescription;
import models.SudokuModel;

/**
 * @author jor
 *
 */
public class SudokuController implements Serializable{
	private static final long serialVersionUID = 7098916606736246465L;
	private String phoneNumber;
	private SudokuModel sModel;

	public SudokuController(String phoneNumber) {
		super();
		this.phoneNumber = phoneNumber;
		File f = new File("./config"+this.phoneNumber+"/savegame.ser");
		if(!f.exists() && f.isDirectory())
			read();
		else
			this.sModel = new SudokuModel();
	}

	public void read(){
		Object ser = new Object();
		try {
			FileInputStream fis = new FileInputStream("./config/"+this.phoneNumber+"/savegame.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			ser = ois.readObject();
			ois.close();
			fis.close();
		} catch (Exception e) {}
		if(ser instanceof SudokuModel){
			sModel = (SudokuModel) ser;
		}
	}

	public void save(){
		try {
			FileOutputStream fos = new FileOutputStream("./config/"+this.phoneNumber+"/savegame.ser");
			ObjectOutputStream oos = new ObjectOutputStream (fos);
			oos.writeObject(sModel);
			oos.close();
			fos.close();
		} catch (Exception e) {}
	}

	public int [][] getBoard(DifficultyDescription diff){
		sModel.setBoard(sModel.nextBoard(diff));
		return this.sModel.getBoard();
	}

	public void setBoard(DifficultyDescription diff){
		sModel.setBoard(sModel.nextBoard(diff));
	}

	public String getSaveTime(){
		return this.sModel.getSaveTime();
	}

	public SudokuModel getsModel() {
		return sModel;
	}
}
