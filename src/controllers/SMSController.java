/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 17.05.2015
 * Datei: SMSController.java
 */
package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.List;
import models.Contact;
import models.SMS;
import models.SMSModel;


/**
 * @author jor
 *
 */
public class SMSController implements Serializable{
	private static final long serialVersionUID = 1588307750824381219L;
	private SMSModel smsModel;
	private String phoneNumber;

	public SMSController(String phoneNumber) {
		super();
		this.phoneNumber = phoneNumber;
		File f = new File("./config/"+this.phoneNumber+"/sms.ser");
		if(f.exists() && !f.isDirectory())
			read();
		else
			this.smsModel = new SMSModel();
	}

	public void read(){
		Object ser = new Object();
		try {
			FileInputStream fis = new FileInputStream("./config/"+this.phoneNumber+"/sms.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			ser = ois.readObject();
			ois.close();
			fis.close();
		} catch (Exception e) {}
		if(ser instanceof SMSModel)
			smsModel = (SMSModel) ser;
	}

	public void save(){
		try {
			FileOutputStream fos = new FileOutputStream("./config/"+this.phoneNumber+"/sms.ser");
			ObjectOutputStream oos = new ObjectOutputStream (fos);
			oos.writeObject(smsModel);
			oos.close();
			fos.close();
		} catch (Exception e) {}
	}

	public void sendSMS(SMS sms){
		smsModel.sendSMS(sms);
	}

	public int countAllUnreadSMS(){
		return smsModel.countAllUnreadSMS();
	}

	public int countUnreadSMSBetweenConversation(String fromNumber,String numberInList){
		return smsModel.countUnreadSMSBetweenConversation(fromNumber,numberInList);
	}

	public SMS [] getSMSListBetweenConversation(String senderNumber, String receiverNumber){
		return smsModel.getSMSListBetweenConversation(senderNumber, receiverNumber);
	}

	public void readSMS(String phoneNumber){
		smsModel.readSMS(phoneNumber);
	}

	public SMS[] getSMSList(){
		return smsModel.getSMSList();
	}

	public List<Contact> getConversation() {
		return smsModel.getConversation();
	}

	public Contact[] getConversationList() {
		return smsModel.getConversationList();
	}

	public SMSModel getSMSModel() {
		return smsModel;
	}

	public void addConversation(Contact c){
		smsModel.addConversation(c);
		save(); 
	}
	public void removeConversation(Contact c){
		smsModel.removeConversation(c);
		save();	
	}
}
