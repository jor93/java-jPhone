/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 08.05.2015
 * Datei: JPhoneController.java
 */
package controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import JPhone.Init;
import JPhone.JPhone;
import JPhone.Init.Panel;
/**
 * @author jor
 *
 */
public class JPhoneController implements Serializable{
	private static final long serialVersionUID = 6033496465687785365L;
	private boolean firstBoot;
	private ArrayList<JPhone> phoneList;
	private JPhone [] phones;

	public JPhoneController() {
		super();
		this.firstBoot = false;
		File f = new File("./config/phones.ser");
		if(f.exists() && !f.isDirectory()){
			firstBoot = false;
		}else{
			firstBoot = true;
			phoneList = new ArrayList<JPhone>();
			Init.Panel.setPhoneList(phoneList);
		}
		this.phones = null;
	}

	public boolean isFirstBoot() {
		return firstBoot;
	}

	public List<JPhone> getPhoneList() {
		return phoneList;
	}

	public void read(){
		Object ser = new Object();
		try {
			FileInputStream fis = new FileInputStream("./config/phones.ser");
			ObjectInputStream ois = new ObjectInputStream(fis);
			ser = ois.readObject();
			ois.close();
			fis.close();
		} catch (Exception e) {}
		phones = (JPhone[]) ser;
		phoneList = new ArrayList<JPhone>();
		for (int i = 0; i < phones.length; i++) {
			phoneList.add(phones[i]);
		}
		Panel.setPhoneList(phoneList);
	}

	public void save(){
		try {
			FileOutputStream fos = new FileOutputStream("./config/phones.ser");
			ObjectOutputStream oos = new ObjectOutputStream (fos);
			phoneList = Init.Panel.getPhoneList();
			phones = new JPhone[phoneList.size()];
			for (int i = 0; i < phoneList.size(); i++) {
				phones[i] = phoneList.get(i);
			}
			oos.writeObject(phones);
			oos.close();
			fos.close();
		} catch (Exception e) {e.printStackTrace();}
	}	
}
