/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone
 * Datum: 05.05.2015
 * Datei: DifficultyDescription.java
 */
package enums;

/**
 * @author jor
 *
 */
public enum DifficultyDescription{
	 VERYEASY(25),
	 EASY(35),
	 MEDIUM(45),
	 HARD(55);
	 
	private int difficulty;

	private DifficultyDescription(int difficulty) {
		this.difficulty = difficulty;
	}

	public int getDifficulty() {
		return difficulty;
	}		
}