/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 25.05.2015
 * Datei: ColorsDescription.java
 */
package enums;

import java.awt.Color;

/**
 * @author jor
 *
 */
public enum ColorsDescription {
	 RED(new Color(244,67,54),"red"),
	 PINK(new Color(233,30,99),"pink"),
	 PURPLE(new Color(156,39,176),"purple"),
	 DARKPURPLE(new Color(103,58,183),"dark purple"),
	 BLUE(new Color(63,81,181),"blue"),
	 AQUABLUE(new Color(33,150,243),"aqua blue"),
	 LIGHTBLUE(new Color(3,169,244),"light blue"),
	 CYAN(new Color(0,188,212),"cyan"),
	 DARKCYAN(new Color(0,150,136),"dark cyan"),
	 GREEN(new Color(76,175,80),"green"),
	 LIGTHGREEN(new Color(139,195,74),"ligth green"),
	 BILIOUSGREEN(new Color(205,220,57),"bilious green"),
	 YELLOW(new Color(255,235,59),"yellow"),
	 LIGHTORANGE(new Color(255,193,7),"light orange"),
	 ORANGE(new Color(255,152,0),"orange"),
	 DARKORANGE(new Color(255,87,34),"dark orange"),
	 BROWN(new Color(121,85,72),"brown"),
	 GRAY(new Color(158,158,158),"gray"),
	 BLUEGRAY(new Color(96,125,139),"blue gray");
	 
	private Color color;
	private String name;

	private ColorsDescription(Color color, String name) {
		this.color = color;
		this.name = name;
	}

	public Color getColor() {
		return color;
	}	
	
	public String getName(){
		return name;
	}
}
