/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 13.05.2015
 * Datei: AppsName.java
 */
package enums;

/**
 * @author jor
 *
 */
public enum AppsName {
	CONTACT("Contact"), 
	CALCULATOR("Calculator"), 
	SMS("Message"),
	SUDOKU("Sudoku"),
	SETTINGS("Settings");
		
	private String name;

	private AppsName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}	

}
