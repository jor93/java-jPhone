/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 29.04.2015
 * Datei: AppsDescription.java
 */
package enums;

/**
 * @author jor
 *
 */
public enum AppsDescription {

	UTILITY("Utility"), 
	BASIC("Basic function"), 
	GAME("Games");
	
	private String description;

	private AppsDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}	
}
