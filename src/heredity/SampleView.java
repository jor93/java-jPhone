/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 12.05.2015
 * Datei: SampleView.java
 */
package heredity;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.Serializable;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import settings.GlobalSettings;

/**
 * @author jor
 *
 */
@SuppressWarnings("rawtypes")
public abstract class SampleView extends JPanel implements Serializable{
	private static final long serialVersionUID = -1153190464302178511L;
	protected JPanel nav;
	protected JButton add;
	protected JLabel text;
	protected JButton search;
	protected JTextField searchField;
	protected JPanel listing;
	protected JList list;
	private Color color;
	protected String searchQuery;
	protected DefaultListModel model;

	@SuppressWarnings("unchecked")
	public SampleView(Color color, String title, int heightRows, boolean addButtons ,boolean addList) {
		super(null);
		this.color = color;
		this.setBounds(0, 0, GlobalSettings.width, GlobalSettings.height);

		nav = new JPanel(new BorderLayout());
		nav.setBackground(this.color);
		nav.setBounds(0, 0, GlobalSettings.width, 100);

		if(addButtons){
			Click c = new Click();	
			JPanel panel1 = new JPanel(new FlowLayout(FlowLayout.CENTER,0,25));
			panel1.setOpaque(false);
			panel1.setBorder(new EmptyBorder(0, 10, 0, 0));
			add = new JButton("+");
			add.setActionCommand("add");
			add.setPreferredSize(new Dimension(50, 50));
			add.setFont(GlobalSettings.globalFont(35));
			add.setForeground(Color.WHITE);
			add.setBackground(null);
			add.setContentAreaFilled(false);
			add.setBorder(new EmptyBorder(0,0,0,0));
			add.setFocusPainted(false);
			add.addActionListener(c);
			panel1.add(add);
			nav.add(panel1, BorderLayout.WEST);

			JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.CENTER,0,25));
			panel2.setOpaque(false);
			panel2.setBorder(new EmptyBorder(0, 0, 0, 10));
			ImageIcon searchIcon = new ImageIcon(GlobalSettings.sampleViewSearch);
			search = new JButton(searchIcon);
			search.setActionCommand("search");
			search.setBackground(null);
			search.setPreferredSize(new Dimension(50, 50));
			search.setBorder(new EmptyBorder(0,0,0,0));
			search.setFocusPainted(false);
			search.setOpaque(false);
			search.setContentAreaFilled(false);
			search.addActionListener(c);
			panel2.add(search);
			nav.add(panel2, BorderLayout.EAST);	
		}

		text = new JLabel(title);
		text.setPreferredSize(new Dimension(50, 50));
		text.setHorizontalAlignment(SwingConstants.CENTER);
		text.setVerticalAlignment(SwingConstants.CENTER);
		text.setForeground(Color.WHITE);
		text.setFont(GlobalSettings.globalFont(30));
		nav.add(text, BorderLayout.CENTER);
		add(nav);

		listing = new JPanel(null);
		listing.setBounds(0, 95, GlobalSettings.width, GlobalSettings.height-GlobalSettings.heigthControl-95);
		if(addList){
			listing.setBackground(Color.WHITE);
			model = new DefaultListModel();
			list = new JList<>(model);
			list.setFixedCellHeight(heightRows);
			list.setDragEnabled(false);
			list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			list.setLayoutOrientation(JList.VERTICAL);
			JScrollPane scrollPane = new JScrollPane(list,
					ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
					ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPane.setBounds(0, 5, GlobalSettings.width-5, GlobalSettings.height-GlobalSettings.heigthControl-95);
			listing.add(scrollPane,BorderLayout.EAST);
		}
		add(listing);
	}

	private class Click implements ActionListener,Serializable{
		private static final long serialVersionUID = -6312066648691692057L;

		@Override
		public void actionPerformed(ActionEvent e) {
			switch (e.getActionCommand()) {
			case "add":
				add();
				break;
			case "search":
				addSearch();
				add.setVisible(false);
				text.setVisible(false);
				search.setIcon(new ImageIcon(GlobalSettings.sampleViewDelete));
				search.setActionCommand("delete");
				search.setBorder(new EmptyBorder(0,0,20,0));
				nav.add(searchField,BorderLayout.WEST);
				nav.setBorder(new EmptyBorder(10, 20, 20, 0));
				nav.repaint();
				searchField.requestFocus();
				break;
			case "delete":
				searchQuery = "";
				searchField.setText("");
				searchField.setVisible(false);
				add.setVisible(true);
				text.setVisible(true);
				search.setIcon(new ImageIcon(GlobalSettings.sampleViewSearch));
				search.setActionCommand("search");
				search.setBorder(new EmptyBorder(0,0,0,0));
				nav.setBorder(new EmptyBorder(0,0,0,0));
				nav.add(add,BorderLayout.WEST);
				nav.repaint();
				repaint();
				refresh();
				break;
			case "remove":
				remove();
				break;
			}
		}
	}

	private class Search extends KeyAdapter implements Serializable{
		private static final long serialVersionUID = -6312066648691692057L;
		@Override
		public void keyTyped(KeyEvent e) {
			char c = e.getKeyChar();
			if(!(Character.isAlphabetic(c) || c== KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_SPACE || c == KeyEvent.VK_MINUS)){
				e.consume();
				return;
			}
			if(c== KeyEvent.VK_BACK_SPACE){
				if(searchQuery.length() > 0)
					searchQuery = searchQuery.substring(0,searchQuery.length()-1);
				search(searchQuery);
				return;
			}
			searchQuery += c; 
			search(searchQuery);
		}
	}

	public void addSearch(){
		this.searchField = new JTextField();
		this.searchField.setVisible(true);
		this.searchField.setOpaque(false);
		this.searchField.setPreferredSize(new Dimension(350,50));
		this.searchField.setBorder(new MatteBorder(0, 0, 1, 0, Color.BLACK));
		this.searchField.setForeground(Color.WHITE);
		this.searchField.setFont(GlobalSettings.globalFont(30));
		this.searchField.addKeyListener(new Search());
		this.searchQuery = "";
	}

	public void changeToRemove(){
		this.add.setText("");
		this.search.setActionCommand("remove");
		this.search.setIcon(new ImageIcon(GlobalSettings.sampleViewRemove));
	}

	
	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
		nav.setBackground(color);
		nav.repaint();
	}

	public abstract void add();
	public abstract void search(String s);
	public abstract void refresh();
	public abstract void remove(); 
}
