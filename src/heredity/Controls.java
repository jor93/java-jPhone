/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 30.04.2015
 * Datei: Controls.java
 */
package heredity;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import settings.GlobalSettings;

/**
 * @author jor
 *
 */
public abstract class Controls extends JPanel implements Serializable{
	private static final long serialVersionUID = -2524489118278959946L;
	private JButton back;
	private JButton home;
	private JButton lock;
	private boolean show;
	// true = showButtons;
	// false = disableButtons;

	public Controls(boolean show){
		super(null);
		this.show = show;
		this.setSize(GlobalSettings.width,50);
		this.setVisible(true);
		this.setOpaque(true);
		this.setBackground(GlobalSettings.colorControls());

		ImageIcon backIcon = new ImageIcon(GlobalSettings.back);
		back = new JButton();
		back.setVisible(show);
		back.setOpaque(false);
		back.setBackground(new Color(0, 0, 0, 0));
		back.setFocusPainted(false);
		back.setBorderPainted(false);
		back.setContentAreaFilled(false);
		back.setBackground(GlobalSettings.colorControls());
		back.setIcon(backIcon);
		back.setBounds(100,5,40,40);
		back.addActionListener(new BackListener());
		add(back);

		ImageIcon homeIcon = new ImageIcon((GlobalSettings.broMode ? GlobalSettings.brofist :GlobalSettings.home));
		home = new JButton();
		home.setOpaque(false);
		home.setVisible(show);
		home.setFocusPainted(false);
		home.setBorderPainted(false);
		home.setContentAreaFilled(false);
		home.setBackground(GlobalSettings.colorControls());
		home.setIcon(homeIcon);
		home.setBounds(210,5,40,40);
		home.addActionListener(new HomeListener());
		add(home);

		ImageIcon lockIcon = new ImageIcon(GlobalSettings.lock);
		lock = new JButton();
		lock.setVisible(true);
		lock.setOpaque(false);
		lock.setFocusPainted(false);
		lock.setBorderPainted(false);
		lock.setContentAreaFilled(false);
		lock.setBackground(GlobalSettings.colorControls());
		lock.setBounds(310,5,40,40);
		lock.setIcon(lockIcon);
		lock.addActionListener(new LockListener());
		add(lock);	
	}

	public boolean getShowMode() {
		return show;
	}

	public void setShowMode(boolean showMode) {
		this.show = showMode;
		back.setVisible(this.show);
		home.setVisible(this.show);
		lock.setVisible(this.show);
	}

	public void setIconToHome(ImageIcon icon){
		home.setIcon(icon);
		home.repaint();
	}
	
	abstract public void back();
	abstract public void home();
	abstract public void lock();

	private class BackListener implements ActionListener, Serializable{
		private static final long serialVersionUID = 1711877600879934000L;
		@Override
		public void actionPerformed(ActionEvent e) { back(); }
	}

	private class HomeListener implements ActionListener, Serializable{
		private static final long serialVersionUID = -2896284913501649018L;
		@Override
		public void actionPerformed(ActionEvent e) { home(); }
	}

	private class LockListener implements ActionListener, Serializable{
		private static final long serialVersionUID = -7064884877313947935L;

		@Override
		public void actionPerformed(ActionEvent e) { lock(); }
	}
}
