/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 29.04.2015
 * Datei: Apps.java
 */
package heredity;

import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.JPanel;
import enums.AppsDescription;

/**
 * @author jor
 *
 */
public abstract class Apps implements Serializable{
	private static final long serialVersionUID = 7735169344689382741L;
	private String appName;
	private AppsDescription ad;
	private float version;
	protected ArrayList<JPanel> views; 
	public String phoneNumber;
	
	public Apps(String appName, AppsDescription ad, float version, String phoneNumber) {
		super();
		this.appName = appName;
		this.ad = ad;
		this.version = version;
		this.views = new ArrayList<JPanel>();
		this.phoneNumber = phoneNumber;
	}
	
	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public AppsDescription getAd() {
		return ad;
	}

	public void setAd(AppsDescription ad) {
		this.ad = ad;
	}

	public float getVersion() {
		return version;
	}

	public void setVersion(float version) {
		this.version = version;
	}

	public boolean equals(Apps app) {
		return this.getAppName().equals(app.getAppName())
				&& this.getAd().getDescription().equals(app.getAd().getDescription());
	}

	public JPanel getView(int index){
		return views.get(index);
	}

	public void addView(JPanel p){
		views.add(p);
	}
	
	public ArrayList<JPanel> getViews() {
		return views;
	}
	
	public JPanel [] getViewsArray(){
		return views.toArray(new JPanel[views.size()]);
	}

	public void setViews(ArrayList<JPanel> views) {
		this.views = views;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "Apps [appName=" + appName + ", ad=" + ad + ", version="
				+ version + " views=" + views.size() + "]";
	}
}
