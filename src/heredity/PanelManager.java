/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 04.05.2015
 * Datei: PanelManager.java
 */
package heredity;

import javax.swing.JPanel;
import JPhone.JPhone;

/**
 * @author jor
 *
 */
public interface PanelManager{
	public void addPanel(JPanel p); 
	public void removePanel(JPanel p);
	public JPhone getPhone();
}
