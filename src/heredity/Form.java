/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 17.05.2015
 * Datei: Form.java
 */
package heredity;

/**
 * @author jor
 *
 */
public interface Form {
	public void resetText();
}
