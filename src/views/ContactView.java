/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 29.04.2015
 * Datei: ContactView.java
 */
package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.io.Serializable;
import heredity.PanelManager;
import heredity.SampleView;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import enums.AppsName;
import apps.ContactApp;
import models.Contact;
import settings.GlobalSettings;
import JPhone.Init;
import JPhone.JPhone;

/**
 * @author jor
 *
 */
public class ContactView extends SampleView implements PanelManager,Serializable{
	private static final long serialVersionUID = 2329328654392149514L;
	protected String phoneNumber;
	private JList<Contact> list;

	@SuppressWarnings("unchecked")
	public ContactView(String phoneNumber) {
		super(GlobalSettings.contactMainColor(),"Contacts",103,true,true);
		this.phoneNumber = phoneNumber;
		this.list = super.list;
		this.list.setCellRenderer(new Renderer());
		this.setColor(getPhone().getSettingsController().getSettings().getContactMainColor());
	}
	
	public JList<Contact> getList() {
		return list;
	}

	public void setListData(Contact [] c) {
		this.list.setListData(c);
	}

	@Override
	public void add() {
		int index = getPhone().findApp(AppsName.CONTACT.getName());
		if(index != -1){
			if(getPhone().appList.get(index).getViews().size() == 1)
				getPhone().appList.get(index).addView(new ContactViewDetails(phoneNumber,true));
			if(getPhone().getSettingsController().getSettings().isDebugMode())
				System.out.println("jPhone " + getPhone().getPhoneNumber() +" New Contact opened");
			getPhone().addPanel(((ContactApp)(getPhone().appList.get(index))).getView(1));
			((ContactViewDetails)getPhone().appList.get(index).getView(1)).setFocus();
		}
	}

	@Override
	public void search(String s) {
		if(getPhone().getSettingsController().getSettings().isDebugMode())
			System.out.println("jPhone " + getPhone().getPhoneNumber() +" Contact search with query: " + s);
		list.setListData(getPhone().getContactController().getContactModel().search(s));
	}

	@Override
	public void refresh() {
		setListData(getPhone().getContactController().getContactList());
	}

	@Override
	public void remove() {}

	@SuppressWarnings("rawtypes")
	public class Renderer implements ListCellRenderer, Serializable {
		private static final long serialVersionUID = -6387923718968860931L;
		private JPanel container;
		private JLabel name;
		private JLabel phone;

		public Renderer() {
			container = new JPanel(new BorderLayout());
			container.setBackground(Color.WHITE);
			JPanel p = new JPanel();
			p.setBorder(BorderFactory.createEmptyBorder(0,10,0,30));
			p.setLayout(new BorderLayout(20, 20));
			p.setPreferredSize(new Dimension(GlobalSettings.width-20, 103));
			name = new JLabel();
			name.setFont(GlobalSettings.globalFont(20));
			name.setForeground(GlobalSettings.globalFontColorBlack());
			phone = new JLabel();
			phone.setFont(GlobalSettings.globalFont(20));
			phone.setForeground(GlobalSettings.rendererNumber());
			p.setOpaque(false);
			p.add(name, BorderLayout.CENTER);
			p.add(phone, BorderLayout.EAST);
			container.add(p, BorderLayout.CENTER);
			JPanel bottom = new JPanel();
			bottom.setBackground(GlobalSettings.rendererBottom());
			bottom.setPreferredSize(new Dimension(GlobalSettings.width,1));
			container.add(bottom, BorderLayout.SOUTH);
		}

		@Override
		public Component getListCellRendererComponent(final JList list,
				final Object value, final int index,final boolean isSelected,
				final boolean hasFocus) {
			if(index % 2 == 0) {
				container.setBackground(GlobalSettings.rendererList2());
			} else {
				container.setBackground(Color.white);
			}
			Contact c = null;
			if(value instanceof Contact) {
				c = (Contact) value;
			}
			if(isSelected) {
				container.setBackground(GlobalSettings.contactMainColor());
				name.setForeground(GlobalSettings.globalFontColorBlack());
				phone.setForeground(GlobalSettings.globalFontColor());	
				if(getSearchField() != null)
					if(getSearchField().isVisible())
						setSearchText();
				int i = getPhone().findApp(AppsName.CONTACT.getName());
				if(i != -1)
					if(getPhone().appList.get(i).getViews().size() == 2)
						getPhone().appList.get(i).addView(new ContactViewDetails(getPhone().getPhoneNumber(),false));
				if(getPhone().getSettingsController().getSettings().isDebugMode())
					System.out.println("jPhone " + getPhone().getPhoneNumber() +" Contact Details opened");
				((ContactViewDetails) getPhone().appList.get(0).getView(2)).fillGui(c);
				getPhone().addPanel(((ContactApp)(getPhone().appList.get(i))).getView(2));
			} else {
				name.setForeground(GlobalSettings.globalFontColorBlack());
				phone.setForeground(GlobalSettings.rendererNumber());
			}
			if(c != null) {
				c = (Contact) value;
				name.setText(c.getLname()+" "+c.getFname());
				phone.setText(c.getPhoneNumber());
			}
			list.clearSelection();
			return container;
		}
	}

	public void setSearchText(){
		super.searchQuery = super.searchField.getText();
	}

	public String getSearchText(){
		return super.searchQuery;
	}

	public JTextField getSearchField() {
		return super.searchField;
	}

	@Override
	public void addPanel(JPanel p) {
		getPhone().addPanel(p);	
	}

	@Override
	public void removePanel(JPanel p) {}

	@Override
	public JPhone getPhone() {
		return Init.Panel.findJPhone(this.phoneNumber);
	}

}
