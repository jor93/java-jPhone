/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 29.04.2015
 * Datei: LockScreen.java
 */
package views;

import heredity.PanelManager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import JPhone.*;
import settings.GlobalSettings;

/**
 * @author jor
 *
 */
public class LockScreen extends JPanel implements PanelManager,Serializable{
	private static final long serialVersionUID = 3418354225589545895L;
	private JLabel background;
	private JLabel time;
	private JLabel date;
	private JLabel notifications;
	private JButton unlock;
	private String phoneNumber;

	public LockScreen(String phoneNumber) {
		super();
		this.phoneNumber = phoneNumber;
		this.setLayout(null);
		this.setSize(GlobalSettings.width,GlobalSettings.height);
		this.setOpaque(true);

		time = new JLabel("Time");
		time.setForeground(GlobalSettings.globalFontColor());
		time.setFont(GlobalSettings.globalFont(60));
		time.setHorizontalAlignment(SwingConstants.CENTER);
		time.setBounds(0,75,GlobalSettings.width,100);
		add(time);

		date = new JLabel("Date");
		date.setForeground(GlobalSettings.globalFontColor());
		date.setFont(GlobalSettings.globalFont(35));
		date.setHorizontalAlignment(SwingConstants.CENTER);
		date.setBounds(0,150,GlobalSettings.width,100);
		add(date);

		notifications = new JLabel("You have new messages received!");
		if(getPhone().getBgController().getNumber() == 3)
			notifications.setForeground(GlobalSettings.newMessageBright());
		else if(getPhone().getBgController().getNumber() == 1)
			notifications.setForeground(GlobalSettings.newMessageDark());
		else
		notifications.setForeground(GlobalSettings.newMessage());
		notifications.setFont(GlobalSettings.globalFont(22));
		notifications.setBorder(new EmptyBorder(20,55,20,15));
		notifications.setBackground(GlobalSettings.transparentWhite(0));
		notifications.setBounds(0,300,GlobalSettings.width,100);
		notifications.setVisible(false);
		add(notifications);
		
		JPanel transparency = new JPanel();
		transparency.setBounds(0, 500, GlobalSettings.width, 100);
		transparency.setBackground(GlobalSettings.transparentWhite(30));
		add(transparency);

		unlock = new JButton("click to unlock");
		unlock.setFocusPainted(false);
		unlock.setBorderPainted(false);
		unlock.setForeground(GlobalSettings.globalFontColor());
		unlock.setFont(GlobalSettings.globalFont(45));
		unlock.setContentAreaFilled(false);
		unlock.setBounds(0, 500, GlobalSettings.width, 100);
		unlock.addActionListener(new Clicked());
		add(unlock);

		background = new JLabel();
		background.setIcon(getPhone().getBgController().getBg());
		background.setBounds(0,0,GlobalSettings.width,GlobalSettings.height);
		add(background);
		
		Timer timer = new Timer();
		int wait = (60-Calendar.getInstance().get(Calendar.SECOND))*1000;
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				setTimeDate();
			}
		}, wait, 60000);

		setTimeDate();
	}

	
	public void setBg(Icon bg) {
		this.background.setIcon(bg);
		background.repaint();
	}
	
	public JLabel getNotifications() {
		return notifications;
	}

	private class Clicked implements ActionListener,Serializable{
		private static final long serialVersionUID = 1800297676377554538L;
		@Override
		public void actionPerformed(ActionEvent e) {
			if(getPhone().getViewsSize() == 1)
				getPhone().addPanel(new HomeScreen(phoneNumber, getPhone().getAppList().length));
			getPhone().getLatestPanel();
			if(getPhone().getMessages() != 0)
				getPhone().getHomeScreen().setNewMessages(getPhone().getMessages());
			if(getPhone().getSettingsController().getSettings().isDebugMode())
				System.out.println("jPhone " + LockScreen.this.phoneNumber + " unlocked");
		}
	}

	public String getTime(){
		String time = "";
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		Date timestamp = new Date();
		time = sdf.format(timestamp);
		return time;
	}

	public String getDate(){
		String date = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		Date timestamp = new Date();
		date = sdf.format(timestamp);
		return date;
	}

	public void setTimeDate(){
		time.setText(getTime());
		date.setText(getDate());
	}

	public void showNewMessage(boolean show){
		notifications.setVisible(show);
	}

	public void startTimer(){
		Timer timer = new Timer();
		int wait = (60-Calendar.getInstance().get(Calendar.SECOND))*1000;
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				setTimeDate();
			}
		}, wait, 60000);

		setTimeDate();
	}

	@Override
	public void addPanel(JPanel p) {
		getPhone().addPanel(p);
	}

	@Override
	public void removePanel(JPanel p) {}

	@Override
	public JPhone getPhone() {	
		return Init.Panel.findJPhone(phoneNumber);
	}
}
