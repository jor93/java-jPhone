/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 24.05.2015
 * Datei: SettingsView.java
 */
package views;

import heredity.PanelManager;
import heredity.SampleView;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import javax.swing.AbstractButton;
import javax.swing.DefaultButtonModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.plaf.basic.BasicComboBoxEditor;
import enums.ColorsDescription;
import JPhone.Init;
import JPhone.JPhone;
import settings.GlobalSettings;

/**
 * @author jor
 *
 */
@SuppressWarnings("rawtypes")
public class SettingsView extends SampleView implements PanelManager {

	private static final long serialVersionUID = -4072702018224771697L;
	private String phoneNumber;

	private JLabel main;
	private JPanel mainOptions;
	private JLabel backgroundText;
	private JComboBox backgroundSelect; 
	private JLabel debugText;
	private JSwitchBox debugSwitch;
	private JLabel broText;
	private JSwitchBox broSwitch;

	private JLabel color;
	private JPanel colorOptions;
	private JLabel controlText;
	private JColorChooser controlSelect; 
	private JLabel smsText;
	private JColorChooser smsSelect; 
	private JLabel contactText;
	private JColorChooser contactSelect; 
	private JLabel settingsText;
	private JColorChooser settingsSelect;

	private ColorsDescription [] colors;

	public SettingsView(String phoneNumber) {
		super(GlobalSettings.settingsMainColor(), "Settings", 0, false, false);
		this.colors = ColorsDescription.values();
		this.phoneNumber = phoneNumber;
		this.listing.setLayout(new FlowLayout());

		FlowLayout centerLayout = new FlowLayout(FlowLayout.CENTER,0,10);
		MatteBorder border = new MatteBorder(1, 0, 0, 0, GlobalSettings.rendererBottom());
		MatteBorder lastBorder = new MatteBorder(1, 0, 1, 0, GlobalSettings.rendererBottom());

		main = new JLabel("Main Settings");
		main.setPreferredSize(new Dimension(GlobalSettings.width-10,50));
		main.setFont(GlobalSettings.globalFontItalic(25));
		listing.add(main);

		mainOptions = new JPanel(new GridLayout(3,2));
		mainOptions.setPreferredSize(new Dimension(GlobalSettings.width,160));
		mainOptions.setBorder(new EmptyBorder(0, 15, 0, 15));

		backgroundText = new JLabel("Choose Background");
		backgroundText.setFont(GlobalSettings.globalFont(18));
		backgroundText.setBorder(border);
		mainOptions.add(backgroundText);

		JPanel select = new JPanel(centerLayout);
		select.setBorder(border);
		String [] backgrounds = {"Polar Night","Black Forest","Green Leaf","City"};
		backgroundSelect = new JComboBox<>(backgrounds);
		backgroundSelect.setPreferredSize(new Dimension(200,35));
		
		backgroundSelect.setSelectedIndex(getPhone().getBgController().getNumber()-1);
		backgroundSelect.addItemListener(new BGListener());
		select.add(backgroundSelect);
		mainOptions.add(select);

		debugText = new JLabel("Debug-Mode");
		debugText.setFont(GlobalSettings.globalFont(18));
		debugText.setBorder(border);
		mainOptions.add(debugText);

		JPanel panel1 = new JPanel(centerLayout);
		panel1.setBorder(border);
		debugSwitch = new JSwitchBox("on", "off",20,30){
			private static final long serialVersionUID = -4196874895744944601L;

			@Override
			public void on() {
				System.out.println("jPhone " + phoneNumber + " Debug Mode on");
				getPhone().getSettingsController().getSettings().setDebugMode(true);
			}

			@Override
			public void off() {
				System.out.println("jPhone " + phoneNumber + " Debug Mode off");
				getPhone().getSettingsController().getSettings().setDebugMode(false);
			}
		};
		if(getPhone().getSettingsController().getSettings().isDebugMode()){
			debugSwitch.setSelected(true);
			debugSwitch.setOn(true);
		}
		panel1.add(debugSwitch);
		mainOptions.add(panel1);

		broText = new JLabel("Bro-Mode");
		broText.setBorder(lastBorder);
		broText.setFont(GlobalSettings.globalFont(18));
		mainOptions.add(broText);

		JPanel panel2 = new JPanel(centerLayout);
		panel2.setBorder(lastBorder);
		broSwitch = new JSwitchBox("on", "off",20,30){
			private static final long serialVersionUID = 2676050502039352978L;

			@Override
			public void on() {
				if(getPhone().getSettingsController().getSettings().isDebugMode())
					System.out.println("jPhone " + phoneNumber + " Bro Mode on");
				getPhone().getSettingsController().getSettings().setBroMode(true);
				getPhone().getSettingsController().activateBroMode();
			}

			@Override
			public void off() {
				if(getPhone().getSettingsController().getSettings().isDebugMode())
					System.out.println("jPhone " + phoneNumber + " Bro Mode off");
				getPhone().getSettingsController().getSettings().setBroMode(false);
				getPhone().getSettingsController().activateBroMode();
			}
		};
		if(getPhone().getSettingsController().getSettings().isBroMode()){
			broSwitch.setSelected(true);
			broSwitch.setOn(true);
		}
		panel2.add(broSwitch);
		mainOptions.add(panel2);	

		listing.add(mainOptions);

		JPanel colorPanel = new JPanel();
		colorPanel.setBorder(new EmptyBorder(10, 0, 0, 0));
		color = new JLabel("Color Settings");
		color.setPreferredSize(new Dimension(GlobalSettings.width-10,40));
		color.setFont(GlobalSettings.globalFontItalic(25));
		colorPanel.add(color);
		listing.add(colorPanel);

		//index information
		// 19 controls
		// 20 sms
		// 21 contact
		// 22 settings

		FlowLayout selectLayout = new FlowLayout(FlowLayout.CENTER);

		colorOptions = new JPanel(new GridLayout(4,2));
		colorOptions.setPreferredSize(new Dimension(GlobalSettings.width,225));
		colorOptions.setBorder(new EmptyBorder(0, 15, 0, 15));

		controlText = new JLabel("Choose Control");
		controlText.setFont(GlobalSettings.globalFont(18));
		controlText.setBorder(border);
		colorOptions.add(controlText);

		JPanel select1 = new JPanel(selectLayout);
		select1.setBorder(border);
		controlSelect = new JColorChooser(19);
		controlSelect.getColorChooser().addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED){
					int index = getControlSelect().getColorChooser().getSelectedIndex();
					try{
						if(index > 18){
							switch (index) {
							case 19:
								getPhone().getSettingsController().setColorControls(GlobalSettings.colorControls());
								break;
							case 20:
								getPhone().getSettingsController().setColorControls(GlobalSettings.smsMainColor());
								break;
							case 21:
								getPhone().getSettingsController().setColorControls(GlobalSettings.contactMainColor());
								break;
							case 22:
								getPhone().getSettingsController().setColorControls(GlobalSettings.settingsMainColor());
								break;
							}
						}
						else{
							getPhone().getSettingsController().setColorControls(colors[index].getColor());
						}
					} catch(Exception ex){}
				}
			}
		});
		select1.add(controlSelect);
		colorOptions.add(select1);

		smsText = new JLabel("Choose SMS");
		smsText.setFont(GlobalSettings.globalFont(18));
		smsText.setBorder(border);
		colorOptions.add(smsText);

		JPanel select2 = new JPanel(selectLayout);
		select2.setBorder(border);
		smsSelect = new JColorChooser(20);
		smsSelect.getColorChooser().addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED){
					int index = getSmsSelect().getColorChooser().getSelectedIndex();
					try{
						if(index > 18){
							switch (index) {
							case 19:
								getPhone().getSettingsController().setColorSMS(GlobalSettings.colorControls());
								break;
							case 20:
								getPhone().getSettingsController().setColorSMS(GlobalSettings.smsMainColor());
								break;
							case 21:
								getPhone().getSettingsController().setColorSMS(GlobalSettings.contactMainColor());
								break;
							case 22:
								getPhone().getSettingsController().setColorSMS(GlobalSettings.settingsMainColor());
								break;
							}
						}
						else{
							getPhone().getSettingsController().setColorSMS(colors[index].getColor());
						}
					} catch(Exception ex){}
				}
			}
		});
		select2.add(smsSelect);
		colorOptions.add(select2);

		contactText = new JLabel("Choose Contact");
		contactText.setFont(GlobalSettings.globalFont(18));
		contactText.setBorder(border);
		colorOptions.add(contactText);

		JPanel select3 = new JPanel(selectLayout);
		select3.setBorder(border);
		contactSelect = new JColorChooser(21);
		contactSelect.getColorChooser().addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED){
					int index = getContactSelect().getColorChooser().getSelectedIndex();
					try{
						if(index > 18){
							switch (index) {
							case 19:
								getPhone().getSettingsController().setColorContact(GlobalSettings.colorControls());
								break;
							case 20:
								getPhone().getSettingsController().setColorContact(GlobalSettings.smsMainColor());
								break;
							case 21:
								getPhone().getSettingsController().setColorContact(GlobalSettings.contactMainColor());
								break;
							case 22:
								getPhone().getSettingsController().setColorContact(GlobalSettings.settingsMainColor());
								break;
							}
						}
						else{
							getPhone().getSettingsController().setColorContact(colors[index].getColor());
						}
					} catch(Exception ex){}
				}		
			}		
		});
		select3.add(contactSelect);
		colorOptions.add(select3);

		settingsText = new JLabel("Choose Settings");
		settingsText.setFont(GlobalSettings.globalFont(18));
		settingsText.setBorder(lastBorder);
		colorOptions.add(settingsText);

		JPanel select4 = new JPanel(selectLayout);
		select4.setBorder(lastBorder);
		settingsSelect = new JColorChooser(22);
		settingsSelect.getColorChooser().addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED){
					int index = getSettingsSelect().getColorChooser().getSelectedIndex();
					try{
						if(index > 18){
							switch (index) {
							case 19:
								getPhone().getSettingsController().setColorSettings(GlobalSettings.colorControls());
								break;
							case 20:
								getPhone().getSettingsController().setColorSettings(GlobalSettings.smsMainColor());
								break;
							case 21:
								getPhone().getSettingsController().setColorSettings(GlobalSettings.contactMainColor());
								break;
							case 22:
								getPhone().getSettingsController().setColorSettings(GlobalSettings.settingsMainColor());
								break;
							}
						}
						else{
							getPhone().getSettingsController().setColorSettings(colors[index].getColor());
						}
					} catch(Exception ex){}
				}
			}
		});
		select4.add(settingsSelect);
		colorOptions.add(select4);

		listing.add(colorOptions);


		this.setColor(getPhone().getSettingsController().getSettings().getSettingsMainColor());
	}

	public void changeColor(int index){
		try{
			if(index > 18){
				switch (index) {
				case 19:
					getPhone().getSettingsController().setColorSMS(GlobalSettings.colorControls());
					break;
				case 20:
					getPhone().getSettingsController().setColorSMS(GlobalSettings.smsMainColor());
					break;
				case 21:
					getPhone().getSettingsController().setColorSMS(GlobalSettings.contactMainColor());
					break;
				case 22:
					getPhone().getSettingsController().setColorSMS(GlobalSettings.settingsMainColor());
					break;
				}
			}
			else{
				getPhone().getSettingsController().setColorSMS(colors[index].getColor());
			}
		} catch(Exception ex){}
	}

	public class BGListener implements ItemListener,Serializable{
		private static final long serialVersionUID = -5653673622389621502L;

		@Override
		public void itemStateChanged(ItemEvent e) {
			if(e.getStateChange() == ItemEvent.SELECTED){
				int index = getBackgroundSelect().getSelectedIndex()+1;
				switch (index) {
				case 1:
					getPhone().getBgController().updateBG(new ImageIcon(GlobalSettings.backgroundImage1));
					getPhone().getLockScreen().getNotifications().setForeground(GlobalSettings.newMessageDark());
					break;
				case 2:
					getPhone().getBgController().updateBG(new ImageIcon(GlobalSettings.backgroundImage2));
					getPhone().getLockScreen().getNotifications().setForeground(GlobalSettings.newMessage());
					break;
				case 3:
					getPhone().getBgController().updateBG(new ImageIcon(GlobalSettings.backgroundImage3));
					getPhone().getLockScreen().getNotifications().setForeground(GlobalSettings.newMessageBright());
					break;
				case 4:
					getPhone().getBgController().updateBG(new ImageIcon(GlobalSettings.backgroundImage4));
					getPhone().getLockScreen().getNotifications().setForeground(GlobalSettings.newMessage());
					break;
				}
			}		
		}
	}

	public JComboBox getBackgroundSelect() {
		return backgroundSelect;
	}

	public JColorChooser getControlSelect() {
		return controlSelect;
	}

	public JColorChooser getSmsSelect() {
		return smsSelect;
	}

	public JColorChooser getContactSelect() {
		return contactSelect;
	}

	public JColorChooser getSettingsSelect() {
		return settingsSelect;
	}

	@Override
	public void add() {}

	@Override
	public void search(String s) {}

	@Override
	public void refresh() {}

	@Override
	public void remove() {}

	public abstract class JSwitchBox extends AbstractButton{
		private static final long serialVersionUID = 7059570184475754898L;
		private Color colorBright = new Color(220,220,220);
		private Color colorDark = new Color(158,158,158);
		private Color black  = new Color(0,0,0,100);
		private Color white = new Color(255,255,255,100);
		private Color light = new Color(220,220,220);
		private Color red = new Color(239,83,80);
		private Color green = GlobalSettings.contactSave();
		private Font font = new JLabel().getFont();
		private int gap = 5;
		private int globalWitdh = 0;
		private final String trueLabel;
		private final String falseLabel;
		private Dimension thumbBounds;
		//private Rectangle2D bounds;
		private int max;
		private boolean on = false;

		public JSwitchBox(String trueLabel, String falseLabel, int width, int height) {
			this.trueLabel = trueLabel;
			this.falseLabel = falseLabel;
			double trueLenth = getFontMetrics( getFont() ).getStringBounds( trueLabel, getGraphics() ).getWidth()+width;
			double falseLenght = getFontMetrics( getFont() ).getStringBounds( falseLabel, getGraphics() ).getWidth()+width;
			max = (int)Math.max( trueLenth, falseLenght );
			gap =  Math.max( 5, 5+(int)Math.abs(trueLenth - falseLenght ) ); 
			thumbBounds  = new Dimension(max+gap*2,height);
			globalWitdh =  max + thumbBounds.width+gap*2;
			setModel( new DefaultButtonModel() );
			setSelected( false );
			addMouseListener( new MouseAdapter() {
				@Override
				public void mouseReleased( MouseEvent e ) {
					if(new Rectangle( getPreferredSize() ).contains( e.getPoint() )) {
						setSelected( !isSelected() );
						if(on){
							on = false;
							off();
						}else{
							on = true;
							on();
						}
					}
				}
			});
		}

		public boolean isOn() {
			return on;
		}

		public void setOn(boolean on){
			this.on = on;
		}
		
		@Override
		public Dimension getPreferredSize() {
			return new Dimension(globalWitdh, thumbBounds.height);
		}

		@Override
		public void setSelected( boolean b ) {
			if(b){
				setText( trueLabel );
				setBackground( green );
			} else {
				setBackground( red );
				setText( falseLabel );
			}
			super.setSelected( b );
		}
		@Override
		public void setText( String text ) {
			super.setText( text );
		}

		@Override
		public int getHeight() {
			return getPreferredSize().height;
		}

		@Override
		public int getWidth() {
			return getPreferredSize().width;
		}

		@Override
		public Font getFont() {
			return font;
		}

		@Override
		protected void paintComponent( Graphics g ) {
			g.setColor( getBackground() );
			g.fillRoundRect( 1, 1, getWidth()-2 - 1, getHeight()-2 ,2 ,2 );
			Graphics2D g2 = (Graphics2D)g;

			g2.setColor( black );
			g2.drawRoundRect( 1, 1, getWidth()-2 - 1, getHeight()-2 - 1, 2,2 );
			g2.setColor( white );
			g2.drawRoundRect( 1 + 1, 1 + 1, getWidth()-2 - 3, getHeight()-2 - 3, 2,2 );

			int x = 0;
			int lx = 0;
			if(isSelected()) {
				lx = thumbBounds.width;
			} else {
				x = thumbBounds.width;
			}
			int y = 0;
			int w = thumbBounds.width;
			int h = thumbBounds.height;

			g2.setPaint( new GradientPaint(x, (int)(y-0.1*h), colorDark , x, (int)(y+1.2*h), light) );
			g2.fillRect( x, y, w, h );
			g2.setPaint( new GradientPaint(x, (int)(y+.65*h), light , x, (int)(y+1.3*h), colorDark) );
			g2.fillRect( x, (int)(y+.65*h), w, (int)(h-.65*h) );

			if (w>14){
				int size = 10;
				g2.setColor( colorBright );
				g2.fillRect(x+w/2-size/2,y+h/2-size/2, size, size);
				g2.setColor( new Color(120,120,120));
				g2.fillRect(x+w/2-4,h/2-4, 2, 2);
				g2.fillRect(x+w/2-1,h/2-4, 2, 2);
				g2.fillRect(x+w/2+2,h/2-4, 2, 2);
				g2.setColor( colorDark );
				g2.fillRect(x+w/2-4,h/2-2, 2, 6);
				g2.fillRect(x+w/2-1,h/2-2, 2, 6);
				g2.fillRect(x+w/2+2,h/2-2, 2, 6);
				g2.setColor( new Color(170,170,170));
				g2.fillRect(x+w/2-4,h/2+2, 2, 2);
				g2.fillRect(x+w/2-1,h/2+2, 2, 2);
				g2.fillRect(x+w/2+2,h/2+2, 2, 2);
			}

			g2.setColor( black );
			g2.drawRoundRect( x, y, w - 1, h - 1, 2,2 );
			g2.setColor( white );
			g2.drawRoundRect( x + 1, y + 1, w - 3, h - 3, 2,2 );

			g2.setColor( black.darker() );
			g2.setRenderingHint( RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON );
			g2.setFont( getFont() );
			g2.setColor(new Color(255,255,255,200));
			g2.drawString( getText(), lx+gap, y+h/2+h/4 );
		}

		public abstract void on();
		public abstract void off();
	}

	@SuppressWarnings("unchecked")
	public class JColorChooser extends JPanel implements Serializable{
		private static final long serialVersionUID = -407612765964375084L;
		private JComboBox colorChooser;
		private DefaultComboBoxModel model;

		public JColorChooser(int index) {
			super();
			String [][] items = fillArray();
			colorChooser = new JComboBox<>();
			colorChooser.setEditable(true);
			colorChooser.setPreferredSize(new Dimension(200,35));
			colorChooser.setRenderer(new ColorRenderer());
			colorChooser.setEditor(new ColorEditor());
			model = new DefaultComboBoxModel();
			colorChooser.setModel(model);
			addItems(items);
			switch (index) {
			case 19:
				colorChooser.setSelectedIndex(19);
				break;
			case 20:
				colorChooser.setSelectedIndex(20);
				break;
			case 21:
				colorChooser.setSelectedIndex(21);
				break;
			case 22:
				colorChooser.setSelectedIndex(22);
				break;
			}
			add(colorChooser);
		}

		public void addItems(String  [][] items){
			for(String [] item : items){
				model.addElement(item);
			}
		}

		public JComboBox getColorChooser() {
			return colorChooser;
		}

	}

	public class ColorRenderer extends JPanel implements ListCellRenderer,Serializable{
		private static final long serialVersionUID = 7980578565337675074L;
		private JLabel item;

		public ColorRenderer(){
			setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 1.0;
			c.insets = new Insets(2, 2, 2, 2);

			item = new JLabel();
			item.setOpaque(true);
			item.setFont(new Font(item.getFont().getFontName(),Font.PLAIN,15));
			item.setHorizontalAlignment(JLabel.LEFT);

			add(item,c);
			setBackground(Color.LIGHT_GRAY);
		}

		@Override
		public Component getListCellRendererComponent(JList list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {
			String [] name = (String[]) value;
			item.setText(name[0]);
			item.setIcon(new ImageIcon(name[1]));

			if(isSelected){
				item.setBackground(GlobalSettings.rendererNumber());
				item.setForeground(Color.WHITE);
			}else{
				item.setBackground(Color.LIGHT_GRAY);
				item.setForeground(Color.BLACK);
			}
			return this;
		}
	}

	public class ColorEditor extends BasicComboBoxEditor implements Serializable{
		private static final long serialVersionUID = 442999494003687447L;
		private JPanel panel ;
		private JLabel itemText;
		private String selectedValue;

		public ColorEditor() {
			panel = new JPanel(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 1.0;
			c.insets = new Insets(2, 5, 2, 2);
			panel.setBorder(new LineBorder(GlobalSettings.rendererBottom()));
			itemText = new JLabel();
			itemText.setFont(new Font(itemText.getFont().getFontName(),Font.PLAIN,15));
			itemText.setOpaque(false);
			itemText.setHorizontalAlignment(JLabel.LEFT);
			itemText.setForeground(Color.BLACK);

			panel.add(itemText, c);
			panel.setBackground(Color.LIGHT_GRAY);       
		}

		@Override
		public Component getEditorComponent() {
			return this.panel;
		}

		@Override
		public Object getItem() {
			return this.selectedValue;
		}

		@Override
		public void setItem(Object item) {
			if (item == null) {
				return;
			}
			try {
				String [] name = (String[]) item;
				selectedValue = name[0];
				itemText.setText(selectedValue);
				ImageIcon icon = new ImageIcon(name[1]);
				icon.setImage(icon.getImage().getScaledInstance(25, 25, Image.SCALE_DEFAULT));
				itemText.setIcon(icon);
			} catch (Exception e) {}
		}  
	}

	public abstract class ItemListenerColor implements Serializable,ItemListener{
		private static final long serialVersionUID = -3093375430614837624L;
		@Override
		public void itemStateChanged(ItemEvent e) {}
	}

	public String [][] fillArray(){
		String [] names = getNames();
		String [][] array = new String[names.length+4][2];		
		for (int i = 0; i < array.length; i++) {
			for (int j = 0; j < array[i].length; j++) {
				if(i >= array.length-4){
					if(j == 0){
						switch (i) {
						case 19:
							array[i][j] = "default Controls";
							break;
						case 20:
							array[i][j] = "default SMS";
							break;
						case 21:
							array[i][j] = "default Contact";
							break;
						case 22:
							array[i][j] = "default Settings";
							break;
						}
					}else{
						array[i][j] = "./img/settings/color"+i+".png";
						if(i == array.length-1)
							break;
					}
				}else{
					if(j == 0)
						array[i][j] = names[i];
					else
						array[i][j] = "./img/settings/color"+i+".png";
				}
			}
		}
		return array;
	}

	public String [] getNames(){
		String [] names = new String[ColorsDescription.values().length];
		ColorsDescription [] colors = ColorsDescription.values();
		for (int i = 0; i < names.length; i++) {
			names[i] = colors[i].getName();
		}
		return names;
	}

	@Override
	public void addPanel(JPanel p) {
		getPhone().addPanel(p);	
	}

	@Override
	public void removePanel(JPanel p) {}

	@Override
	public JPhone getPhone() {
		return Init.Panel.findJPhone(this.phoneNumber);
	}

}

