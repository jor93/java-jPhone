/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 17.05.2015
 * Datei: SMSView.java
 */
package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.io.Serializable;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import models.Contact;
import apps.SMSApp;
import enums.AppsName;
import JPhone.Init;
import JPhone.JPhone;
import settings.GlobalSettings;
import heredity.PanelManager;
import heredity.SampleView;

/**
 * @author jor
 *
 */
public class SMSView extends SampleView implements PanelManager{
	private static final long serialVersionUID = -4030344206415142303L;
	private String phoneNumber;
	private JList<Contact> list;
	private JLabel newMessages;

	@SuppressWarnings("unchecked")
	public SMSView(String phoneNumber) {
		super(GlobalSettings.smsMainColor(), "Message", 103, true, true);
		this.phoneNumber = phoneNumber;
		this.list = super.list;
		this.list.setCellRenderer(new Renderer());
		if(getPhone().getSMSController().getConversation() != null){
			refresh();
		}
		this.setColor(getPhone().getSettingsController().getSettings().getSmsMainColor());
	}

	public void setNewMessages(int newMessages) {
		if(newMessages == 0) 
			this.newMessages.setText("");
		else
			this.newMessages.setText(String.valueOf(newMessages));
	}

	public JList<Contact> getList() {
		return list;
	}

	@SuppressWarnings("rawtypes")
	public class Renderer implements ListCellRenderer, Serializable {
		private static final long serialVersionUID = -6387923718968860931L;
		private JPanel container;
		private JLabel name;

		public Renderer() {
			container = new JPanel(new BorderLayout());
			container.setBackground(Color.WHITE);
			JPanel p = new JPanel();
			p.setBorder(BorderFactory.createEmptyBorder(0,10,0,30));
			p.setLayout(new BorderLayout(20, 20));
			p.setPreferredSize(new Dimension(GlobalSettings.width-20, 103));
			name = new JLabel();
			name.setFont(GlobalSettings.globalFont(20));
			name.setForeground(GlobalSettings.globalFontColorBlack());
			newMessages = new JLabel();
			newMessages.setFont(GlobalSettings.globalFont(25));
			newMessages.setForeground(GlobalSettings.rendererNumber());
			p.setOpaque(false);
			p.add(name, BorderLayout.CENTER);
			p.add(newMessages, BorderLayout.EAST);
			container.add(p, BorderLayout.CENTER);
			JPanel bottom = new JPanel();
			bottom.setBackground(GlobalSettings.rendererBottom());
			bottom.setPreferredSize(new Dimension(GlobalSettings.width,1));
			container.add(bottom, BorderLayout.SOUTH);
		}

		@Override
		public Component getListCellRendererComponent(final JList list,
				final Object value, final int index,final boolean isSelected,
				final boolean hasFocus) {				
			if(index % 2 == 0) {
				container.setBackground(GlobalSettings.rendererList2());
			} else {
				container.setBackground(Color.white);
			}
			Contact c = null;
			if(value instanceof Contact) {
				c = (Contact) value;
			}
			if(isSelected) {
				container.setBackground(GlobalSettings.smsMainColor());
				name.setForeground(Color.white);
				newMessages.setForeground(Color.white);	
				int i = getPhone().findApp(AppsName.SMS.getName());
				if(i != -1){
					getPhone().appList.get(i).addView(new SMSViewConversation(phoneNumber,c));
					if(getPhone().getSettingsController().getSettings().isDebugMode())
						System.out.println("jPhone " + getPhone().getPhoneNumber() +" Conversation opened with " + (c.getLname().compareToIgnoreCase("")==0?c.getPhoneNumber() :c.getLname()+" "+c.getFname()));
					int panel = 0;
					for (int j = 1; j < getPhone().appList.get(i).getViews().size(); j++) {
						if(((SMSViewConversation)getPhone().appList.get(i).getView(j)).getContact().equals(c)){
							panel = j;
						}				
					}
					getPhone().addPanel(((SMSApp)(getPhone().appList.get(i))).getView(panel));
					if(newMessages.getText().compareToIgnoreCase("") != 0){
						getPhone().getSMSController().readSMS(c.getPhoneNumber());
						setNewMessages(getPhone().getSMSController().countUnreadSMSBetweenConversation(phoneNumber, c.getPhoneNumber()));
						getPhone().getHomeScreen().setNewMessages(getPhone().getSMSController().countAllUnreadSMS());
						getPhone().setMessages(getPhone().getSMSController().countAllUnreadSMS());
					}
					((SMSViewConversation)getPhone().getCurrentPanel()).setFocusOnText();
				}
			} else {
				name.setForeground(GlobalSettings.globalFontColorBlack());
				newMessages.setForeground(GlobalSettings.rendererNumber());
			}
			if(c != null) {
				c = (Contact) value;
				if(c.getLname().compareToIgnoreCase("")==0)
					name.setText(c.getPhoneNumber());
				else
					name.setText(c.getLname()+" "+c.getFname());
				int count = getPhone().getSMSController().countUnreadSMSBetweenConversation(phoneNumber, c.getPhoneNumber());
				setNewMessages(count);
			}
			list.clearSelection();
			return container;
		}
	}

	@Override
	public void add() {
		int index = getPhone().findApp(AppsName.SMS.getName());
		if(index != -1){
			getPhone().appList.get(index).addView(new SMSViewSelect(phoneNumber));
			if(getPhone().getSettingsController().getSettings().isDebugMode())
				System.out.println("jPhone " + getPhone().getPhoneNumber() +" Select Contact opened");
			getPhone().addPanel(((SMSApp)(getPhone().appList.get(index))).getView(getPhone().appList.get(index).getViews().size()-1));
			((SMSViewSelect)getPhone().getCurrentPanel()).setList(getPhone().getContactController().getContactList());
		}
	}

	@Override
	public void search(String s) {
		list.setListData(getPhone().getSMSController().getSMSModel().searchConversations(s));
	}

	@Override
	public void refresh() {
		list.setListData(getPhone().getSMSController().getConversationList());
	}

	@Override
	public void remove() {}

	@Override
	public void addPanel(JPanel p) {
		getPhone().addPanel(p);
	}

	@Override
	public void removePanel(JPanel p) {}

	@Override
	public JPhone getPhone() {	
		return Init.Panel.findJPhone(phoneNumber);
	}

}
