/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone
 * Datum: 05.05.2015
 * Datei: SudokuViewGame.java
 */
package views;

import heredity.PanelManager;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import enums.DifficultyDescription;
import JPhone.Init;
import JPhone.JPhone;
import settings.GlobalSettings;
import models.SudokuModel;

/**
 * @author jor
 *
 */

public class SudokuViewGame extends JPanel implements PanelManager{

	private static final long serialVersionUID = -8787191272135656716L;
	private SudokuModel generator;
	private int [][] board;
	private int [][] board2;
	private int [][] result;
	private Timer timer;
	private String phoneNumber;

	private Color lightGray;
	private Color gray;
	private Font font;
	private Font italic;
	private JPanel info;
	private JLabel [] labels;
	private JButton start;
	private final String [] NAMESLABEL = {"Difficulty: ","text","Time: ","time"};
	private JCheckBox help;

	private JPanel game;
	private JPanel [] gameFields;
	private JTextField [][] fields;
	private MatteBorder gridBorder1;
	private MatteBorder gridBorder2;
	private MatteBorder gridBorder3;

	private GridLayout gameLayout;

	private JPanel button;
	private JButton [] buttons;
	private final String [] NAMESBUTTONS = {"New","Reset","Solve","Check"};

	public class ActionEvents extends MouseAdapter implements ActionListener,ItemListener,Serializable{
		private static final long serialVersionUID = 735890744267870971L;
		private Color blue = GlobalSettings.sudokuHover();
		private String zahl = "0";

		@Override
		public void mouseClicked(MouseEvent e) {
			for (int i = 0; i < fields.length; i++) {
				for (int j = 0; j < fields[i].length; j++) {
					if(e.getSource() == fields[i][j]){
						zahl = fields[i][j].getText();
						if(zahl.compareToIgnoreCase("") == 0){
							zahl ="0";
							break;
						}
					}				
				}
			}
			setHover(zahl);
		}

		@Override
		public void mouseExited(MouseEvent e) {	
			for (int i = 0; i < fields.length; i++) {
				for (int j = 0; j < fields[i].length; j++) {
					if(fields[i][j].getText().compareToIgnoreCase("")==0 ){
						fields[i][j].setBackground(Color.WHITE);
						break;
					}
				}
			}
			resetHover(zahl);
			zahl = "0";
		}

		private void setHover(String zahl){		
			if(zahl == "0") return;
			for (int i = 0; i < fields.length; i++) {
				for (int j = 0; j < fields[i].length; j++) {
					if(fields[i][j].getText().compareToIgnoreCase(zahl) == 0){
						fields[i][j].setBackground(blue);
					}
				}
			}
		}

		private void resetHover(String zahl){
			for (int i = 0; i < fields.length; i++) {
				for (int j = 0; j < fields[i].length; j++) {
					if(fields[i][j].getText().compareToIgnoreCase(zahl) == 0 || fields[i][j].getText().compareToIgnoreCase("") == 0){
						if(board[i][j] != 0){
							fields[i][j].setEditable(false);
							fields[i][j].setBackground(gray);
						}else{
							fields[i][j].setBackground(Color.WHITE);
						}
					}
				}
			}
		}


		@Override
		public void actionPerformed(ActionEvent e) {			
			Object element = e.getSource();
			if(element instanceof JButton){
				switch (e.getActionCommand()) {
				case "start":
					startTimer();
					break;
				case "New":
					board = getPhone().getSudokuController().getsModel().nextBoard(generator.getDiff());
					if(timer != null)
						stopTimer();
					labels[3].setText("00:00:00");	
					board2 = generator.copyBoard(board);
					fillGui(true);
					if(help.isSelected() == true) addHoverListener();
					if(getPhone().getSettingsController().getSettings().isDebugMode())
						System.out.println("jPhone " + phoneNumber + " Sudoku new Game: " +(generator.getDiff()==DifficultyDescription.VERYEASY ? "very easy" : generator.getDiff().toString().toLowerCase()) + " opened" );
					break;
				case "Reset":
					if(generator.isFinished(board)) return;
					if(generator.isIdent(board, board2)) return;
					int reply = (JOptionPane.showConfirmDialog(game, "Do you really want to reset?","Reset",JOptionPane.YES_NO_OPTION));
					if(reply == JOptionPane.YES_OPTION){
						resetGui();
						fillGui(false);	
					}
					if(getPhone().getSettingsController().getSettings().isDebugMode())
						System.out.println("jPhone " + phoneNumber + " Sudoku game reset");
					break;
				case "Solve":
					if(generator.isFinished(board)) return;
					result = generator.copyBoard(board);
					generator.solveSudoku(board);					
					for (int j = 0; j < fields.length; j++) {
						for (int j2 = 0; j2 < fields[j].length; j2++) {
							if(result[j][j2] != 0){
								fields[j][j2].setText(board[j][j2]+"");
								fields[j][j2].setEditable(false);
								fields[j][j2].setBackground(gray);
							}else{
								fields[j][j2].setText(board[j][j2]+"");
								fields[j][j2].setBackground(Color.WHITE);
								fields[j][j2].setEditable(true);
							}
						}
					}
					removeHoverListener();
					generator.solveSudoku(board);
					stopTimer();
					if(getPhone().getSettingsController().getSettings().isDebugMode())
						System.out.println("jPhone " + phoneNumber + " Sudoku game: " + (generator.getDiff()==DifficultyDescription.VERYEASY ? "very easy" : generator.getDiff().toString().toLowerCase())+" solved by computer" );
					break;
				case "Check":
					if(generator.isFinished(board)) return;
					if(generator.isFinished(board2)){
						result = generator.copyBoard(board);
						generator.solveSudoku(result);	
						if(generator.isIdent(board2, result)){
							stopTimer();
							JOptionPane.showMessageDialog(game, "Congratulations! You solved the sudoku in " + labels[3].getText());
							removeHoverListener();
							generator.solveSudoku(board);
							if(getPhone().getSettingsController().getSettings().isDebugMode())
								System.out.println("jPhone " + phoneNumber + " Sudoku game: " + (generator.getDiff()==DifficultyDescription.VERYEASY ? "very easy" : generator.getDiff().toString().toLowerCase())+" solved correctly in " + labels[3].getText());
						}
						else{
							JOptionPane.showMessageDialog(game,"Sudoku is not solved correctly!","Error", JOptionPane.ERROR_MESSAGE);
							if(getPhone().getSettingsController().getSettings().isDebugMode())
								System.out.println("jPhone " + phoneNumber + " Sudoku game: " + (generator.getDiff()==DifficultyDescription.VERYEASY ? "very easy" : generator.getDiff().toString().toLowerCase())+" not solved correctly");
						}
					} else{
						JOptionPane.showMessageDialog(game,"Please fill the gaps!","Error", JOptionPane.ERROR_MESSAGE);
					}
					break;
				}
			}
		}

		@Override
		public void itemStateChanged(ItemEvent e) {
			if(e.getStateChange() == ItemEvent.DESELECTED){
				for (int j = 0; j < fields.length; j++) {
					for (int j2 = 0; j2 < fields[j].length; j2++) {
						fields[j][j2].removeMouseListener(events);
					}
				}
			}else{
				for (int j = 0; j < fields.length; j++) {
					for (int j2 = 0; j2 < fields[j].length; j2++) {
						if(!generator.isFinished(board))
							fields[j][j2].addMouseListener(events);
					}
				}
			}
		}
	}

	public class KeyListeners extends KeyAdapter implements Serializable{
		private static final long serialVersionUID = 3352371401454344391L;

		@Override
		public void keyTyped(KeyEvent e) {
			char c = e.getKeyChar();
			if(!(Character.isDigit(c) || c== KeyEvent.VK_BACK_SPACE)  || c == KeyEvent.VK_0)
				e.consume();
		}
	} 

	ActionEvents events = new ActionEvents();
	KeyListeners onlyNumbers = new KeyListeners();

	public SudokuViewGame(String phoneNumber){
		super(null);
		this.phoneNumber = phoneNumber;
		this.setBounds(0, 0, GlobalSettings.width, GlobalSettings.height-GlobalSettings.heightPanel);

		font = GlobalSettings.globalFont(20);
		italic = GlobalSettings.globalFontItalic(20);

		lightGray = new Color(180,180,180);
		gray = new Color(232,232,230);

		int height = SudokuModel.getBoardHeight();
		int width =SudokuModel.getBoardWidth();

		generator = getPhone().getSudokuController().getsModel();

		info = new JPanel(new GridLayout(3,4,0,0));
		info.setBounds(0,0,GlobalSettings.width,100);
		info.setBorder(new EmptyBorder(5, 10, 0, 0));
		labels = new JLabel[NAMESLABEL.length];
		for (int i = 0; i < NAMESLABEL.length; i++) {
			labels[i] = new JLabel(NAMESLABEL[i]);
			if(i % 2 == 0) labels[i].setFont(font);
			else labels[i].setFont(italic);
			info.add(labels[i]);
		}

		help = new JCheckBox("Enable Hover Help");
		help.setSelected(true);
		help.addItemListener(events);
		info.add(help);

		labels[1].setText("");
		labels[1].setBorder(new EmptyBorder(0, 10, 0, 0));
		labels[3].setText("00:00:00");

		start = new JButton("start");
		start.setVisible(false);
		start.addActionListener(events);

		gameLayout = new GridLayout(3,3,0,0);
		game = new JPanel(gameLayout);
		game.setBounds(0,100,449,425);
		gameFields = new JPanel[height];
		gridBorder1 = new MatteBorder(1,1,1,1,lightGray);
		gridBorder2 = new MatteBorder(0,0,1,1,lightGray);
		gridBorder3 = new MatteBorder(0,0,0,1,lightGray);
		for (int i = 0; i < gameFields.length; i++) {
			gameFields[i] = new JPanel(gameLayout);
			gameFields[i].setBorder(gridBorder1);
			game.add(gameFields[i]);
		}

		fields = new JTextField[height][width];
		for (int j = 0; j < fields.length; j++) {
			for (int j2 = 0; j2 < fields[j].length; j2++) {
				fields[j][j2] = new JTextField(1);
				fields[j][j2].setHorizontalAlignment(JTextField.CENTER);
				fields[j][j2].setFont(font);
				fields[j][j2].addMouseListener(events);
				if(j2 == fields[j].length) fields[j][j2].setBorder(gridBorder3); 
				else fields[j][j2].setBorder(gridBorder2);
				switch (j) {
				case 2:
				case 5:
				case 8:
					fields[j][j2].setBorder(gridBorder3);
					break;
				}		
			}
		}

		button = new JPanel(new GridLayout(1,4,15,100));
		button.setBounds(0, 520, 449, 90);
		EmptyBorder empty = new EmptyBorder(30, 10, 10, 10);
		button.setBorder(empty);
		buttons = new JButton[NAMESBUTTONS.length];
		for (int i = 0; i < NAMESBUTTONS.length; i++) {
			buttons[i] = new JButton(NAMESBUTTONS[i]);
			buttons[i].setFont(font);
			buttons[i].setFocusPainted(false);
			buttons[i].addActionListener(events);
			button.add(buttons[i]);
		}

		add(info);
		add(game);
		add(button);

	}

	public void addHoverListener(){
		for (int j = 0; j < fields.length; j++) {
			for (int j2 = 0; j2 < fields[j].length; j2++) {
				fields[j][j2].addMouseListener(events);
			}
		}
	}

	public void removeHoverListener(){
		for (int j = 0; j < fields.length; j++) {
			for (int j2 = 0; j2 < fields[j].length; j2++) {
				fields[j][j2].removeMouseListener(events);
			}
		}
	}

	public void resetGui(){
		for (int j = 0; j < fields.length; j++) {
			for (int j2 = 0; j2 < fields[j].length; j2++) {
				fields[j][j2].setText("");
			}
		}
	}

	public void resetErrors(){
		for (int j = 0; j < fields.length; j++) {
			for (int j2 = 0; j2 < fields[j].length; j2++) {
				if(fields[j][j2].getBackground().getRGB() == GlobalSettings.sudokuVerifier().getRGB()){
					fields[j][j2].setText("");
					fields[j][j2].setBackground(Color.WHITE);
					break;
				}
			}
		}
	}

	public int[][] getBoard2() {
		return board2;
	}

	public void fillGui(boolean startTimer){
		if(startTimer)
			start.doClick();
		board = getPhone().getSudokuController().getsModel().getBoard();
		board2 = generator.copyBoard(board);
		generator.setBoard2(board2);
		String s = "";
		switch (getPhone().getSudokuController().getsModel().getDiff()) {
		case VERYEASY:
			s = "very easy";
			break;
		case EASY:
			s = "easy";
			break;
		case MEDIUM:
			s = "medium";
			break;
		case HARD:
			s = "hard";
			break;
		}
		labels[1].setText(s);
		for (int j = 0; j < fields.length; j++) {
			for (int j2 = 0; j2 < fields[j].length; j2++) {
				if(board[j][j2] != 0){
					fields[j][j2].setText(board[j][j2]+"");
					fields[j][j2].setEditable(false);
					fields[j][j2].setBackground(gray);
					fields[j][j2].setInputVerifier(null);
				}else{
					fields[j][j2].setText("");
					fields[j][j2].setInputVerifier(new Verifier());
					fields[j][j2].addKeyListener(onlyNumbers);
					fields[j][j2].setBackground(Color.WHITE);
					fields[j][j2].setEditable(true);
				}
			}
		}

		int cell = 0;
		int row = 0;
		for (int i = 0; i < gameFields.length; i++) {
			if(i < 3) row = 0;
			else if(i > 2 && i < 6) row = 3;
			else row = 6;
			for (int j = 0; j < fields.length/3; j++) {
				switch (i) {
				case 0:
				case 3:
				case 6:
					cell = 0; 
					break;
				case 1:
				case 4:
				case 7:
					cell = 3; 
					break;
				case 2:
				case 5:
				case 8:
					cell = 6; 
					break;
				}
				for (int j2 = 0; j2 < fields[j].length/3; j2++) {
					gameFields[i].add(fields[row][cell]);
					cell++;
				}
				row++;
			}
		}
	}

	public void startTimer(){
		timer = new Timer();
		Time time = new Time();
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				labels[3].setText(time.doTime());
			}
		}, 0, 1000);
	}

	public void startTimer(String s){
		if(generator.isFinished(board)) return;
		timer = new Timer();
		String [] times = s.split(":");
		Time time = new Time(Integer.parseInt(times[2]),Integer.parseInt(times[1]),Integer.parseInt(times[0]));
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				labels[3].setText(time.doTime());
			}
		}, 0, 1000);
	}

	public void saveTime(){
		resetErrors();
		getPhone().getSudokuController().getsModel().setSaveTime(labels[3].getText());
		stopTimer();
	}

	public void stopTimer(){
		if(timer != null){
			timer.cancel();
			timer.purge();
			timer = null;
		}
	}

	public class Time {
		private DecimalFormat df = new DecimalFormat("00");
		private String secs = "";
		private int countSecs;
		private String mins = "";
		private int countMins;
		private String hours = "";
		private int countHours;

		public Time(){
			super();
			this.countSecs = 0;
			this.countMins = 0;
			this. countHours = 0;
		}

		public Time(int countSecs, int countMins, int countHours) {
			super();
			this.countSecs = countSecs;
			this.countMins = countMins;
			this.countHours = countHours;
		}

		public String doTime() {
			countSecs++;
			if(countSecs<=59) {	
				secs = df.format(countSecs);
			} else {
				countSecs = 0;	
				secs = df.format(countSecs);
				countMins++;
			}
			if(countMins<=59) {
				mins = df.format(countMins);
			} else {
				countMins = 0;	
				mins = df.format(countMins);
				countHours++;
			}
			hours = df.format(countHours);
			return hours + ":" + (countMins <= 59 ? mins:df.format(0)) + ":" + (countSecs <= 59 ? secs : df.format(0));
		}
	}

	public class Verifier extends InputVerifier implements Serializable{
		private static final long serialVersionUID = 408780063012004210L;

		@Override
		public boolean verify(JComponent input) {
			removeHoverListener();
			if(((JTextField)input).getText().matches("[1-9]") || ((JTextField)input).getText().compareToIgnoreCase("") == 0){
				addHoverListener();
				for (int j = 0; j < fields.length; j++) {
					for (int j2 = 0; j2 < fields[j].length; j2++) {
						if(fields[j][j2] == ((JTextField)input)){
							if(((JTextField)input).getText().compareToIgnoreCase("")==0)
								board2[j][j2] = 0;
							else
								board2[j][j2] = Integer.valueOf(((JTextField)input).getText());
							break;
						}
					}
				}
				return true;
			}
			return false;
		}

		@Override
		public boolean shouldYieldFocus(JComponent input) {
			if(!verify(input)){	
				input.setBackground(GlobalSettings.sudokuVerifier());			
				return false;
			}
			input.setBackground(Color.WHITE);
			return true;
		}
	} 

	@Override
	public void addPanel(JPanel p) {}

	@Override
	public void removePanel(JPanel p) {}

	@Override
	public JPhone getPhone() {
		return Init.Panel.findJPhone(this.phoneNumber);
	}
}
