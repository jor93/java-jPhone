/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 05.05.2015
 * Datei: SudokuViewDifficulty.java
 */
package views;

import heredity.PanelManager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.*;

import apps.SudokuApp;
import JPhone.Init;
import JPhone.JPhone;
import settings.GlobalSettings;
import enums.AppsName;
import enums.DifficultyDescription;

/**
 * @author jor
 *
 */
public class SudokuViewDifficulty extends JPanel implements PanelManager{
	private static final long serialVersionUID = -8526063137624794721L;
	private JButton [] buttons;
	private String [] names = {"Very easy","Easy","Medium", "Hard"};
	private String phoneNumber;

	private class ActionEvents implements ActionListener,Serializable{
		private static final long serialVersionUID = 4074503335385466198L;

		@Override
		public void actionPerformed(ActionEvent e) {
			int index = getPhone().findApp(AppsName.SUDOKU.getName());
			if(getPhone().appList.get(index).getViews().size() == 2)
				getPhone().appList.get(index).addView(new SudokuViewGame(phoneNumber));
			JPanel p = ((SudokuApp)(getPhone().appList.get(index))).getView(2);
			switch (e.getActionCommand()) {
			case "Very easy":
				if(getPhone().getSettingsController().getSettings().isDebugMode())
				System.out.println("jPhone " + getPhone().getPhoneNumber() +" Sudoku new Game: very easy opened");
				if(index != -1){
					getPhone().getSudokuController().setBoard(DifficultyDescription.VERYEASY);
					((SudokuViewGame)(p)).fillGui(true);
					getPhone().addPanel(p);
				}
				break;
			case "Easy":
				if(getPhone().getSettingsController().getSettings().isDebugMode())
				System.out.println("jPhone " + getPhone().getPhoneNumber() +" Sudoku new Game: easy opened");
				if(index != -1){
					getPhone().getSudokuController().setBoard(DifficultyDescription.EASY);
					((SudokuViewGame)(p)).fillGui(true);
					getPhone().addPanel(p);
				}
				break;
			case "Medium":
				if(getPhone().getSettingsController().getSettings().isDebugMode())
				System.out.println("jPhone " + getPhone().getPhoneNumber() +" Sudoku new Game: medium opened");
				if(index != -1){
					getPhone().getSudokuController().setBoard(DifficultyDescription.MEDIUM);
					((SudokuViewGame)(p)).fillGui(true);
					getPhone().addPanel(p);
				}
				break;
			case "Hard":
				if(getPhone().getSettingsController().getSettings().isDebugMode())
				System.out.println("jPhone " + getPhone().getPhoneNumber() +" Sudoku new Game: hard opened");
				if(index != -1){
					getPhone().getSudokuController().setBoard(DifficultyDescription.HARD);
					((SudokuViewGame)(p)).fillGui(true);
					getPhone().addPanel(p);
				}
				break;
			}
		}
	}

	ActionEvents actionEvent = new ActionEvents();

	public SudokuViewDifficulty(String phoneNumber){
		super(null);
		this.phoneNumber = phoneNumber;
		this.setBounds(0, 0, GlobalSettings.width, GlobalSettings.height);

		JLabel text = new JLabel(new ImageIcon(GlobalSettings.sudokuBackgroundText));
		text.setBounds(0, 0, GlobalSettings.width, 200);
		add(text);

		JLabel background = new JLabel(new ImageIcon(GlobalSettings.sudokuBackground));
		background.setBounds(0, 75, GlobalSettings.width, 600);
		add(background);

		buttons = new JButton[names.length];
		int space = 100;
		int start = 225; 
		for (int i = 0; i < buttons.length; i++) {
			buttons[i] = new JButton(names[i]);
			buttons[i].setFont(GlobalSettings.globalFont(25));
			buttons[i].setFocusPainted(false);
			buttons[i].addActionListener(actionEvent);
			buttons[i].setBounds(100, start += (i == 0 ? 0 : space), 250, 75);
			setComponentZOrder(buttons[i], 0);
		}
	}

	@Override
	public void addPanel(JPanel p) {
		getPhone().addPanel(p);	
	}

	@Override
	public void removePanel(JPanel p) {}

	@Override
	public JPhone getPhone() {
		return Init.Panel.findJPhone(this.phoneNumber);
	}
}
