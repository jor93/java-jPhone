/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 17.05.2015
 * Datei: SMSViewConversation.java
 */
package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import heredity.PanelManager;
import heredity.SampleView;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import models.Contact;
import models.SMS;
import JPhone.Init;
import JPhone.JPhone;
import settings.GlobalSettings;

/**
 * @author jor
 *
 */
public class SMSViewConversation extends SampleView implements PanelManager,Serializable{

	private static final long serialVersionUID = -1173286534304755598L;
	private final int MAX = GlobalSettings.height-GlobalSettings.heigthControl-95-125;
	private String phoneNumber;
	private JTextArea text;
	private JButton send;
	private JPanel panel;
	private Contact contact;
	private JPanel flow;
	private JScrollPane flowScrollPane;

	public SMSViewConversation(String phoneNumber, Contact contact) {
		super(GlobalSettings.smsMainColor(), (contact.getLname().compareToIgnoreCase("")==0 ? contact.getPhoneNumber() :contact.getLname()+" "+contact.getFname()), 100, true, false);
		this.phoneNumber = phoneNumber;
		this.contact = contact;
		changeToRemove();
		super.listing.setBounds(0,95,GlobalSettings.width, GlobalSettings.height-GlobalSettings.heigthControl-95-125);
		super.listing.setBackground(Color.WHITE);

		this.flow = new JPanel();
		this.flow.setLayout(new BoxLayout(flow, BoxLayout.Y_AXIS));
		this.flow.setBounds(0,0,GlobalSettings.width, 0);
		this.flow.setOpaque(false);
		this.flow.setAlignmentY(JPanel.TOP_ALIGNMENT);
		flowScrollPane = new JScrollPane(flow,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		flowScrollPane.setBounds(0, 5, GlobalSettings.width-5, GlobalSettings.height-GlobalSettings.heigthControl-95-125);
		flowScrollPane.setBorder(BorderFactory.createEmptyBorder(5,0,0,0));
		listing.add(flowScrollPane);

		panel = new JPanel(new BorderLayout());
		panel.setBounds(0, 490, GlobalSettings.width, 125);
		panel.setBorder(new MatteBorder(1, 0, 0, 0, new Color(150,150,150)));

		JPanel panel1 = new JPanel(new BorderLayout());
		panel1.setPreferredSize(new Dimension(GlobalSettings.width-100,125));
		text = new JTextArea();
		text.setMargin(new Insets(10, 10, 10, 10));
		text.setLineWrap(true);
		text.setWrapStyleWord(true);
		text.setFont(GlobalSettings.globalFont(20));
		JScrollPane scrollPane2 = new JScrollPane(text,
				ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane2.setBorder(null);
		panel1.add(scrollPane2,BorderLayout.CENTER);
		panel.add(panel1,BorderLayout.WEST);

		JPanel panel2 = new JPanel(new GridBagLayout());
		panel2.setBorder(new MatteBorder(0, 1, 0, 0, new Color(150,150,150)));
		panel2.setPreferredSize(new Dimension(100,125));
		panel2.setBackground(Color.WHITE);
		send = new JButton("Send");
		send.setPreferredSize(new Dimension(75,40));
		send.setVerticalAlignment(SwingConstants.CENTER);
		send.setHorizontalTextPosition(SwingConstants.CENTER);
		send.setFocusPainted(false);
		send.setFont(GlobalSettings.globalFont(18));
		send.setBorder(new LineBorder(new Color(150,150,150)));
		send.setBackground(new Color(92,107,192));
		send.setForeground(Color.WHITE);
		send.addActionListener(new Send());
		panel2.add(send);
		panel.add(panel2,BorderLayout.CENTER);
		add(panel);

		if(getPhone().getSMSController().getSMSListBetweenConversation(phoneNumber, contact.getPhoneNumber()) != null){
			fillGui();
		}
		
		this.setColor(getPhone().getSettingsController().getSettings().getSmsMainColor());
	}

	public class Send implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			switch (e.getActionCommand()) {
			case "Send":
				if(text.getText().length() < 1){
					JOptionPane.showMessageDialog(listing,"Please write some text!","Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				SMS sms = new SMS(phoneNumber, contact, text.getText(), new Date());
				File f = new File("./config/"+contact.getPhoneNumber());
				getPhone().getSMSController().sendSMS(sms);
				if(f.exists() && f.isDirectory()){
					JPhone receivePhone = getReceiverPhone(contact.getPhoneNumber());
					receivePhone.getSMSController().sendSMS(sms);
					if(getPhone().getSettingsController().getSettings().isDebugMode())
						System.out.println("jPhone "+phoneNumber+" SMS send to " + contact.getPhoneNumber() + " with text: " + sms.getText());
					receivePhone.setMessages(getReceiverPhone(contact.getPhoneNumber()).getSMSController().countAllUnreadSMS());
					if(receivePhone.getViewsSize() > 1)
						receivePhone.getHomeScreen().setNewMessages(getReceiverPhone(contact.getPhoneNumber()).getMessages());	
					int size = receivePhone.getSMSController().getConversation().size();
					List<Contact> list = receivePhone.getSMSController().getConversation();
					List<Contact> contactList = receivePhone.getContactController().getContacts();
					boolean exist = false;
					for (int i = 0; i < size; i++) {
						if(list.get(i).getPhoneNumber().compareToIgnoreCase(phoneNumber)==0){
							exist = true;
							break;
						}
					}
					if(!exist){
						for (int i = 0; i < contactList.size(); i++) {
							if(contactList.get(i).getPhoneNumber().compareToIgnoreCase(phoneNumber)==0){
								exist = true;
								receivePhone.getSMSController().addConversation(contactList.get(i));
								break;
							}
						}
					}
					if(!exist)
						receivePhone.getSMSController().getConversation().add(new Contact("","",phoneNumber));
					if(receivePhone.getCurrentPanel() instanceof SMSView)
						((SMSView)receivePhone.getCurrentPanel()).refresh();
					if(receivePhone.getCurrentPanel() instanceof SMSViewConversation){
						((SMSViewConversation)receivePhone.getCurrentPanel()).fillGui();
						receivePhone.getSMSController().readSMS(phoneNumber);
						receivePhone.getSMSController().readSMS(contact.getPhoneNumber());
						((SMSView)receivePhone.appList.get(1).getView(0)).refresh();
						receivePhone.setMessages(receivePhone.getSMSController().countAllUnreadSMS());
						receivePhone.getHomeScreen().setNewMessages(receivePhone.getSMSController().countAllUnreadSMS());
						receivePhone.getLockScreen().showNewMessage((receivePhone.getSMSController().countAllUnreadSMS() == 0 ? false : true));
					}
					if(getPhone().getSettingsController().getSettings().isDebugMode())
						System.out.println("jPhone " + contact.getPhoneNumber() + " has received "+ receivePhone.getSMSController().countAllUnreadSMS() + " new messages");
				}
				else{
					if(getPhone().getSettingsController().getSettings().isDebugMode())
						System.out.println("jPhone "+phoneNumber+" can not send SMS");
				}
				text.setText("");
				fillGui();
				break;
			}
		}	
	}

	public void fillGui(){
		flow.removeAll();
		SMS[] sms = getPhone().getSMSController().getSMSListBetweenConversation(phoneNumber, contact.getPhoneNumber());
		int space = 0;
		for(int i = 0; i < sms.length; i++) {
			JPanel message = new JPanel(new FlowLayout((sms[i].getSendTo().getPhoneNumber().equals(phoneNumber) ? FlowLayout.LEFT : FlowLayout.RIGHT), 0,0));
			message.setPreferredSize(new Dimension(300, 1));
			message.setOpaque(false);
			JTextArea conversation =  new JTextArea();
			conversation.setText(sms[i].getText());
			conversation.setSize(GlobalSettings.width-100, 200);	
			conversation.setEditable(false);
			conversation.setMargin(new Insets(10, 10, 10, 10));
			conversation.setFont(GlobalSettings.globalFont(18));
			conversation.setLineWrap(true);
			conversation.setWrapStyleWord(true);
			conversation.setForeground(sms[i].getSendTo().getPhoneNumber().equals(phoneNumber) ? Color.BLACK : Color.WHITE);
			conversation.setBackground(sms[i].getSendTo().getPhoneNumber().equals(phoneNumber) ? new Color(180,180,180) : new Color(0,191,255));
			message.add(conversation);
			flow.add(message);
			flow.repaint();
			message.repaint();
			message.setSize(new Dimension(GlobalSettings.width-100, conversation.getPreferredSize().height));
			message.setPreferredSize(new Dimension(GlobalSettings.width-100, conversation.getPreferredSize().height));
			conversation.setPreferredSize(new Dimension(GlobalSettings.width-100,conversation.getPreferredSize().height));
			space += conversation.getPreferredSize().height+20;
		}
		flow.setPreferredSize(new Dimension(GlobalSettings.width, space));
		flowScrollPane.setBounds(0, 5, GlobalSettings.width-5, (space+20 < MAX ? space+20 : MAX));
		flowScrollPane.repaint();
		JScrollBar vertical = flowScrollPane.getVerticalScrollBar();
		vertical.setValue( vertical.getMaximum()+20);
		flow.repaint();
	}


	public void setFocusOnText(){
		text.requestFocus();
	}

	public Contact getContact() {
		return contact;
	}

	@Override
	public void add() {}

	@Override
	public void search(String s) {}

	@Override
	public void refresh() {}

	@Override
	public void remove() {
		if(getPhone().getSettingsController().getSettings().isDebugMode())
			System.out.println("jPhone " + getPhone().getPhoneNumber() +" Conversation deleted with " + (contact.getLname().compareToIgnoreCase("")==0?contact.getPhoneNumber():contact.getLname()+" "+contact.getFname()));
		getPhone().getSMSController().removeConversation(contact);
		getPhone().removeLastPanel();
		((SMSView)getPhone().appList.get(1).getView(0)).refresh();
	}

	@Override
	public void addPanel(JPanel p) {
		getPhone().addPanel(p);	
	}

	@Override
	public void removePanel(JPanel p) {}

	@Override
	public JPhone getPhone() {
		return Init.Panel.findJPhone(this.phoneNumber);
	}

	public JPhone getReceiverPhone(String phoneNumber){
		return Init.Panel.findJPhone(phoneNumber);
	}

}
