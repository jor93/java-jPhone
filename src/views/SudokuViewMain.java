/**
 * Autor: Johner Robert
 * Modul: Temp 
 * Datum: 31.03.2015
 * Datei: SudokuModel.java
 */
package views;

import heredity.PanelManager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.Serializable;

import javax.swing.*;

import enums.AppsName;
import JPhone.Init;
import JPhone.JPhone;
import settings.GlobalSettings;
import apps.SudokuApp;

/**
 * @author jor
 *
 */
public class SudokuViewMain extends JPanel implements PanelManager{
	private static final long serialVersionUID = 1L;
	private JButton [] buttons;
	private final String [] NAMES = {"New Game","Resume"};
	private String phoneNumber;

	private class ActionEvents implements ActionListener,Serializable{
		private static final long serialVersionUID = 311886088009043712L;

		@Override
		public void actionPerformed(ActionEvent e) {
			Object element = e.getSource();
			if (element instanceof JButton){
				int index = getPhone().findApp(AppsName.SUDOKU.getName());
				switch (e.getActionCommand()) {
				case "New Game":
					if(getPhone().getSettingsController().getSettings().isDebugMode())
						System.out.println("jPhone " + getPhone().getPhoneNumber() +" Sudoku Difficulty opened");
					if(index != -1){
						if(getPhone().appList.get(index).getViews().size() == 1)
							getPhone().appList.get(index).addView(new SudokuViewDifficulty(phoneNumber));
						getPhone().addPanel(((SudokuApp)(getPhone().appList.get(index))).getView(1));
					}
					break;
				case "Resume":
					if(index != -1){
						if(getPhone().getSettingsController().getSettings().isDebugMode())
							System.out.println("jPhone " + getPhone().getPhoneNumber() +" Savegame opened");
						getPhone().addPanel(((SudokuApp)(getPhone().appList.get(index))).getView(1));
						JPanel p = ((SudokuApp)(getPhone().appList.get(index))).getView(2);
						getPhone().getSudokuController().read();
						((SudokuViewGame)(p)).startTimer(getPhone().getSudokuController().getsModel().getSaveTime());
						getPhone().addPanel(p);
					}
					break;
				}
			}
		}
	}

	public SudokuViewMain(String phoneNumber){
		super(null);
		this.phoneNumber = phoneNumber;
		this.setBounds(0, 0, GlobalSettings.width, GlobalSettings.height);
		this.setOpaque(true);

		ActionEvents actionEvent = new ActionEvents();

		JLabel text = new JLabel(new ImageIcon(GlobalSettings.sudokuBackgroundText));
		text.setBounds(0, 0, GlobalSettings.width, 200);
		add(text);

		JLabel background = new JLabel(new ImageIcon(GlobalSettings.sudokuBackground));
		background.setBounds(0, 75, GlobalSettings.width, 600);
		add(background);

		buttons = new JButton[NAMES.length];

		for (int i = 0; i < buttons.length; i++) {
			buttons[i] = new JButton(NAMES[i]);
			buttons[i].setFont(GlobalSettings.globalFont(30));
			buttons[i].setFocusPainted(false);
			buttons[i].addActionListener(actionEvent);
			buttons[i].setBounds(100,230,250,75);
			if(i == 1){ 
				buttons[i].setBounds(100,400,250,75);
				File f = new File("./config/"+this.phoneNumber+"/savegame.ser");
				if(f.exists() && !f.isDirectory())
					buttons[1].setEnabled(true);
				else
					buttons[i].setEnabled(false);
			}
			setComponentZOrder(buttons[i], 0);
		}	
	}

	public void checkSavegame(){
		File f = new File("./config/"+getPhone().getPhoneNumber()+"/savegame.ser");
		if(f.exists() && !f.isDirectory())
			buttons[1].setEnabled(true);
	}

	@Override
	public void addPanel(JPanel p) {
		getPhone().addPanel(p);	
	}

	@Override
	public void removePanel(JPanel p) {}

	@Override
	public JPhone getPhone() {
		return Init.Panel.findJPhone(this.phoneNumber);
	}
}
