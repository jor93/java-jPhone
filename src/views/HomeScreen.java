/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 29.04.2015
 * Datei: HomeScreen.java
 */
package views;

import heredity.PanelManager;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import enums.AppsName;
import apps.CalculatorApp;
import apps.ContactApp;
import apps.SMSApp;
import apps.SettingsApp;
import apps.SudokuApp;
import JPhone.Init;
import JPhone.JPhone;
import settings.GlobalSettings;

/**
 * @author jor
 *
 */
public class HomeScreen extends JPanel implements PanelManager,Serializable{
	private static final long serialVersionUID = -6496851067111483448L;
	private String phoneNumber;
	private JLabel background;
	private Icon bg;
	private ContactIcon contact;
	private CalculatorIcon calculator;
	private MessageIcon sms;
	private SudokuIcon sudoku;
	private SettingsIcon settings;

	public HomeScreen(String phoneNumber, int anzApps) {
		super(null);
		this.phoneNumber = phoneNumber;
		setBounds(0, 0, GlobalSettings.width, GlobalSettings.height);
		setOpaque(true);

		bg = getPhone().getBgController().getBg();

		background = new JLabel();
		background.setIcon(bg);
		background.setBounds(0,0,GlobalSettings.width,GlobalSettings.height-GlobalSettings.heigthControl);
		add(background);

		anzApps = (anzApps%2==0 ? anzApps/2 : (anzApps/2)+1);
		JPanel apps = new JPanel(new GridLayout(anzApps, 2, 5, 5));
		apps.setBounds(90, 90, 335, 490);
		apps.setOpaque(false);
		if(getPhone().appInList(AppsName.SMS.getName())){
			sms = new MessageIcon(AppsName.SMS);
			apps.add(sms);
		}
		if(getPhone().appInList(AppsName.CONTACT.getName())){
			contact  = new ContactIcon(AppsName.CONTACT);
			apps.add(contact);
		}
		if(getPhone().appInList(AppsName.CALCULATOR.getName())){
			calculator = new CalculatorIcon(AppsName.CALCULATOR);
			apps.add(calculator);
		}
		if(getPhone().appInList(AppsName.SUDOKU.getName())){
			sudoku = new SudokuIcon(AppsName.SUDOKU);
			apps.add(sudoku);
		}
		if(getPhone().appInList(AppsName.SETTINGS.getName())){
			settings = new SettingsIcon(AppsName.SETTINGS);
			apps.add(settings);
		}
		setComponentZOrder(apps, 0);
	}


	public MessageIcon getSms() {
		return sms;
	}

	public void setBg(Icon bg) {
		this.bg = bg;
		background.setIcon(bg);
		background.repaint();
	}

	private abstract class HomeAppsIcons extends JPanel{
		private static final long serialVersionUID = -8267062372412624694L;
		private JLabel app;
		private JButton startApp;
		private ImageIcon icon;

		public HomeAppsIcons(AppsName appName){
			super(null);
			setSize(GlobalSettings.widthApp, 135);
			setOpaque(false);
			app = new JLabel(appName.getName());
			app.setForeground(GlobalSettings.globalFontColor());
			app.setFont(GlobalSettings.globalFont(20));
			app.setHorizontalAlignment(SwingConstants.CENTER);
			app.setLocation(0,110);
			app.setSize(GlobalSettings.widthApp, 25);
			add(app);

			switch (appName) {
			case CALCULATOR:
				icon = new ImageIcon(GlobalSettings.calculator);
				break;
			case CONTACT:
				icon = new ImageIcon(GlobalSettings.contact);
				break;
			case SMS:
				icon = new ImageIcon(GlobalSettings.sms);
				break;
			case SUDOKU:
				icon = new ImageIcon(GlobalSettings.sudoku);
				break;
			case SETTINGS:
				icon = new ImageIcon(GlobalSettings.settings);
				break;
			}

			startApp = new JButton();
			startApp.setLocation(0, 10);
			startApp.setBorder(null);
			startApp.setBorderPainted(false);
			startApp.setContentAreaFilled(false);
			startApp.setSize(GlobalSettings.widthApp, GlobalSettings.widthApp);
			startApp.setIcon(icon);
			startApp.setOpaque(false);
			startApp.setFocusPainted(false);
			startApp.addActionListener(new Click());
			add(startApp);
		}

		private class Click implements ActionListener,Serializable{
			private static final long serialVersionUID = -7220164772543398748L;
			@Override
			public void actionPerformed(ActionEvent e) {
				showApp();
			}
		}

		public abstract void showApp();
	}

	private class ContactIcon extends HomeAppsIcons{
		private static final long serialVersionUID = 5916954620446379972L;

		public ContactIcon(AppsName appName) {
			super(appName);
		}

		@Override
		public void showApp() {
			if(getPhone().getSettingsController().getSettings().isDebugMode())
				System.out.println("jPhone " + getPhone().getPhoneNumber() +" Contact opened");
			int index = getPhone().findApp(AppsName.CONTACT.getName());
			if(index != -1)
				if(getPhone().appList.get(index).getViews().size() == 0)
					getPhone().appList.get(index).addView(new ContactView(phoneNumber));
			getPhone().addPanel(((ContactApp)(getPhone().appList.get(index))).getView(0));
		}
	}

	private class CalculatorIcon extends HomeAppsIcons{
		private static final long serialVersionUID = -5363841132549725740L;
		public CalculatorIcon(AppsName appName) {
			super(appName);
		}

		@Override
		public void showApp() {
			if(getPhone().getSettingsController().getSettings().isDebugMode())
				System.out.println("jPhone " + getPhone().getPhoneNumber() +" Calculator opened");
			int index = getPhone().findApp(AppsName.CALCULATOR.getName());
			if(index != -1){
				if(getPhone().appList.get(index).getViews().size() == 0)
					getPhone().appList.get(index).addView(new CalculatorView(phoneNumber));
				getPhone().addPanel(((CalculatorApp)(getPhone().appList.get(index))).getView(0));
			}
		}
	}

	private class MessageIcon extends HomeAppsIcons{
		private static final long serialVersionUID = -5264245130876933242L;
		private JLabel notification;
		private ImageIcon icon;
		private JLabel anzMessage;

		public MessageIcon(AppsName appName) {
			super(appName);
			notification = new JLabel();
			notification.setSize(30,30);
			notification.setLocation(80,0);
			icon = new ImageIcon(GlobalSettings.smsNotification);
			notification.setIcon(icon);
			notification.setVisible(false);
			add(notification);

			anzMessage = new JLabel("");
			anzMessage.setForeground(GlobalSettings.globalFontColor());
			anzMessage.setFont(GlobalSettings.globalFont(20));
			anzMessage.setLocation(90,0);
			anzMessage.setSize(30, 30);
			add(anzMessage);

			setComponentZOrder(anzMessage, 0);
			setComponentZOrder(notification, 1);
		}

		public void setAnzMessage(int count) {
			if(count == 0){
				notification.setVisible(false);
				anzMessage.setText("");
				return;
			}
			notification.setVisible(true);
			if(count > 9)
				anzMessage.setLocation(83, 0);
			else
				anzMessage.setLocation(90,0);
			if(count > 99)
				anzMessage.setText("99");
			else
				anzMessage.setText(String.valueOf(count));
		}

		@Override
		public void showApp() {	
			if(getPhone().getSettingsController().getSettings().isDebugMode())
				System.out.println("jPhone " + getPhone().getPhoneNumber() +" Message opened");
			int index = getPhone().findApp(AppsName.SMS.getName());
			if(index != -1){
				if(getPhone().appList.get(index).getViews().size() == 0)
					getPhone().appList.get(index).addView(new SMSView(phoneNumber));
				getPhone().addPanel(((SMSApp)(getPhone().appList.get(index))).getView(0));
			}
		}

	}

	private class SudokuIcon extends HomeAppsIcons{
		private static final long serialVersionUID = 3292694917880142179L;

		public SudokuIcon(AppsName appName) {
			super(appName);
		}

		@Override
		public void showApp() {
			if(getPhone().getSettingsController().getSettings().isDebugMode())
				System.out.println("jPhone " + getPhone().getPhoneNumber() +" Sudoku Main opened");
			int index = getPhone().findApp(AppsName.SUDOKU.getName());
			if(index != -1)
				if(getPhone().appList.get(index).getViews().size() == 0)
					getPhone().appList.get(index).addView(new SudokuViewMain(phoneNumber));
			getPhone().addPanel(((SudokuApp)(getPhone().appList.get(index))).getView(0));
		}
	}

	private class SettingsIcon extends HomeAppsIcons{
		private static final long serialVersionUID = 3292694917880142179L;

		public SettingsIcon(AppsName appName) {
			super(appName);
		}

		@Override
		public void showApp() {
			if(getPhone().getSettingsController().getSettings().isDebugMode())
				System.out.println("jPhone " + getPhone().getPhoneNumber() +" Settings opened");
			int index = getPhone().findApp(AppsName.SETTINGS.getName());
			if(index != -1)
				if(getPhone().appList.get(index).getViews().size() == 0)
					getPhone().appList.get(index).addView(new SettingsView(phoneNumber));
			getPhone().addPanel(((SettingsApp)(getPhone().appList.get(index))).getView(0));
		}
	}

	public void setNewMessages(int count){
		getSms().setAnzMessage(count);
	}

	@Override
	public void addPanel(JPanel p) {
		getPhone().addPanel(p);
	}

	@Override
	public void removePanel(JPanel p) {
	}

	@Override
	public JPhone getPhone() {
		return Init.Panel.findJPhone(phoneNumber);
	}

}
