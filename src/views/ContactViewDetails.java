/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 29.04.2015
 * Datei: ContactViewDetails.java
 */
package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.Serializable;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import models.Contact;
import JPhone.Init;
import JPhone.JPhone;
import heredity.Form;
import heredity.PanelManager;
import heredity.SampleView;
import settings.GlobalSettings;

/**
 * @author jor
 *
 */
public class ContactViewDetails extends SampleView implements PanelManager,Form{
	private static final long serialVersionUID = 4778372088468290742L;
	private static final String EMAILCHECKER = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private String phoneNumber;
	private JLabel fname;
	private JTextField fnameText;
	private JLabel lname;
	private JTextField lnameText;
	private JLabel number;
	private JTextField numberText;
	private JLabel email;
	private JTextField emailText;
	private JButton save;
	private JButton delete;
	protected JPanel form;
	protected Contact oldContact;
	private boolean viewMode;
	private int counter = 0;
	//true new
	//false update

	public ContactViewDetails(String phoneNumber, boolean viewMode) {
		super(GlobalSettings.contactMainColor(),"",100,false,false);
		this.phoneNumber = phoneNumber;
		this.viewMode = viewMode;

		JPanel p = super.listing;
		p.setLayout(new BorderLayout());

		form = new JPanel(new GridLayout(4,2,20,40));
		form.setPreferredSize(new Dimension(GlobalSettings.width,400));
		form.setBorder(new EmptyBorder(20,15,0,15));
		fname = new JLabel("First name: ");
		fname.setFont(GlobalSettings.globalFont(20));
		form.add(fname);
		fnameText = new JTextField(35);
		fnameText.setFont(GlobalSettings.globalFont(20));
		fnameText.addKeyListener(new NameChecker());
		form.add(fnameText);
		lname = new JLabel("Last name: ");
		lname.setFont(GlobalSettings.globalFont(20));
		form.add(lname);
		lnameText = new JTextField(30);
		lnameText.setFont(GlobalSettings.globalFont(20));
		lnameText.addKeyListener(new NameChecker());
		form.add(lnameText);

		number = new JLabel("Phone number: ");
		number.setFont(GlobalSettings.globalFont(20));
		form.add(number);
		numberText = new JTextField(40);
		numberText.setFont(GlobalSettings.globalFont(20));
		numberText.addKeyListener(new PhoneNumberChecker());
		form.add(numberText);

		email = new JLabel("E-Mail: ");
		email.setFont(GlobalSettings.globalFont(20));
		form.add(email);
		emailText = new JTextField(25);
		emailText.setFont(GlobalSettings.globalFont(17));
		form.add(emailText);

		p.add(form,BorderLayout.NORTH);

		JPanel buttons = new JPanel(new GridLayout(2,1,0,20));
		buttons.setPreferredSize(new Dimension(GlobalSettings.width,100));
		buttons.setBorder(new EmptyBorder(10,125,20,125));
		ImageIcon icon = new ImageIcon(GlobalSettings.contactSave);
		save = new JButton("save");
		save.setBackground(GlobalSettings.contactSave());
		save.setIcon(icon);
		save.setFont(GlobalSettings.globalFont(20));
		save.setForeground(GlobalSettings.globalFontColor());
		save.setFocusPainted(false);
		save.setBorder(new LineBorder(Color.GRAY,1));
		save.addActionListener(new Send());
		buttons.add(save);
		ImageIcon icon2 = new ImageIcon(GlobalSettings.contactDelete);
		delete = new JButton("delete");
		delete.setIcon(icon2);
		delete.setBackground(GlobalSettings.contactDelete());
		delete.setFocusable(false);
		delete.setFont(GlobalSettings.globalFont(20));
		delete.addActionListener(new Send());
		delete.setForeground(GlobalSettings.globalFontColor());
		delete.setBorder(new LineBorder(Color.GRAY,1));
		buttons.add(delete);
		p.add(buttons);

		setViewMode(this.viewMode);
		this.setColor(getPhone().getSettingsController().getSettings().getContactMainColor());
	}

	private class PhoneNumberChecker extends KeyAdapter implements Serializable{
		private static final long serialVersionUID = 3921742320813847133L;
		@Override
		public void keyTyped(KeyEvent e) {
			char c = e.getKeyChar();
			if(c== KeyEvent.VK_BACK_SPACE){
				counter--;
				if(counter < 0){
					counter = 0;
					e.consume();
				}		
				return;
			}			
			if(counter > 12) {e.consume();return;}
			if(!(Character.isDigit(c) || c== KeyEvent.VK_BACK_SPACE)){
				e.consume();
				return;
			} 

			if(counter == 3 || counter == 7 || counter == 10){
				numberText.setText(numberText.getText()+" ");
				counter += 2;
			}else
				counter++;
		}
	}

	private class NameChecker extends KeyAdapter implements Serializable{
		private static final long serialVersionUID = -9072137333038969714L;
		@Override
		public void keyTyped(KeyEvent e) {
			char c = e.getKeyChar();
			if(!(Character.isAlphabetic(c) || c== KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_SPACE || c == KeyEvent.VK_MINUS)){
				e.consume();
				return;
			}
		}
	}

	private class Send implements ActionListener,Serializable{
		private static final long serialVersionUID = 260123364461986706L;
		@Override
		public void actionPerformed(ActionEvent e) {
			switch (e.getActionCommand()) {
			case "save":
				if(fnameText.getText().length() < 1 || lnameText.getText().length() < 1 || numberText.getText().length() < 1){
					JOptionPane.showMessageDialog(form,"Please fill all informationen!","Error", JOptionPane.ERROR_MESSAGE);
					return;
				}
				if(numberText.getText().length() < 13){
					JOptionPane.showMessageDialog(form,"Phone Number Format Error!","Error",JOptionPane.ERROR_MESSAGE); 
					return;
				}
				if(emailText.getText().length() > 1){
					if(!(emailText.getText().matches(EMAILCHECKER))){
						JOptionPane.showMessageDialog(form,"Incorrect E-Mail!","Error", JOptionPane.ERROR_MESSAGE);
						return;
					}			
				}
				if(viewMode){
					if(getPhone().getContactController().alreadyInList(numberText.getText())){
						JOptionPane.showMessageDialog(form,"Phone Number already exists!","Error",JOptionPane.ERROR_MESSAGE);
						return;
					}
					if(emailText.getText().length() < 1)
						getPhone().getContactController().addContact(new Contact(fnameText.getText().trim(),lnameText.getText().trim(),numberText.getText()));
					else
						getPhone().getContactController().addContact(new Contact(fnameText.getText().trim(),lnameText.getText().trim(),numberText.getText(),emailText.getText()));
					resetText();
				}else{
					Contact newContact = null;
					if(emailText.getText().length() < 1)
						newContact = new Contact(fnameText.getText().trim(),lnameText.getText().trim(),numberText.getText());
					else
						newContact = new Contact(fnameText.getText().trim(),lnameText.getText().trim(),numberText.getText(),emailText.getText());
					if(getPhone().getContactController().alreadyInList(newContact.getPhoneNumber()) && oldContact.getPhoneNumber().compareToIgnoreCase(newContact.getPhoneNumber())!=0){
						JOptionPane.showMessageDialog(form,"Phone Number already exists!","Error",JOptionPane.ERROR_MESSAGE);
						return;
					}
					getPhone().getContactController().updateContact(oldContact, newContact);
				}
				counter = 0;
				getPhone().removeLastPanel();
				if(viewMode == false){
					if(((ContactView)getPhone().appList.get(0).getView(0)).getSearchField() != null){
						if(((ContactView)getPhone().appList.get(0).getView(0)).getSearchField().isVisible()){
							((ContactView) getPhone().appList.get(0).getView(0)).setListData(getPhone().getContactController().getContactModel().search(getSearchText()));
						}
					} else
						((ContactView) getPhone().appList.get(0).getView(0)).setListData(getPhone().getContactController().getContactList());
				}else
					((ContactView) getPhone().appList.get(0).getView(0)).setListData(getPhone().getContactController().getContactList());
				break;
			case "delete":
				getPhone().getContactController().removeContact(oldContact);
				getPhone().removeLastPanel();
				((ContactView) getPhone().appList.get(0).getView(0)).setListData(getPhone().getContactController().getContactList());
				break;
			}
		}
	}
	
	@Override
	public void resetText(){
		fnameText.setText("");
		lnameText.setText("");
		numberText.setText("");
		emailText.setText("");
	}
	
	public String getSearchText(){
		String s = ((ContactView)getPhone().appList.get(0).getView(0)).getSearchText();
		return s;
	}

	public void setFocus(){
		fnameText.requestFocus();
	}

	public void setOldContact(Contact c){
		this.oldContact = c;	
	}

	public void setTitle(String s){
		super.text.setText(s);
	}

	public boolean isViewMode() {
		return viewMode;
	}

	public void fillGui(Contact c){
		setOldContact(c);
		fnameText.setText(c.getFname());
		lnameText.setText(c.getLname());
		numberText.setText(c.getPhoneNumber());
		if(c.getEmail().length() < 1)
			emailText.setText("");
		else
			emailText.setText(c.getEmail());
	}

	public void setViewMode(boolean viewMode) {
		this.viewMode = viewMode;
		if(this.viewMode){
			setTitle("New Contact");
			delete.setVisible(false);
		}
		else{
			setTitle("Update Contact");
			delete.setVisible(true);
		}
		super.repaint();
		repaint();

	}


	@Override
	public void add() {}

	@Override
	public void search(String s) {}

	@Override
	public void refresh() {}
	
	@Override
	public void remove() {}

	@Override
	public void addPanel(JPanel p) {
		getPhone().addPanel(p);	
	}

	@Override
	public void removePanel(JPanel p) {}

	@Override
	public JPhone getPhone() {
		return Init.Panel.findJPhone(this.phoneNumber);
	}
}
