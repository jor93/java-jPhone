/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 05.05.2015
 * Datei: CalculatorView.java
 */
package views;

import heredity.PanelManager;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import javax.swing.*;
import javax.swing.border.MatteBorder;
import JPhone.Init;
import JPhone.JPhone;
import settings.GlobalSettings;
import models.Calculator;

/**
 * @author jor
 *
 */

public class CalculatorView extends JPanel implements PanelManager{
	private static final long serialVersionUID = 4114808397218560095L;
	private JPanel view;
	private JTextField content;
	private String contentText;

	private JPanel options;
	private JButton [] buttons;

	private final char [] OPERATORS = {'÷','×','−','+'};
	private final String [] NAMES = {"Delete","«","7","8","9",Character.toString(OPERATORS[0]),"4","5","6",Character.toString(OPERATORS[1]),"1","2","3",Character.toString(OPERATORS[2]),".","0","=",Character.toString(OPERATORS[3])};

	private Color lightGray;
	private MatteBorder bButton;

	private boolean showResult = false;
	private boolean showInt= false;
	private double result;

	protected String phoneNumber;

	private class ActionEvents implements ActionListener,Serializable{			
		private static final long serialVersionUID = -3792915663465584384L;

		@Override
		public void actionPerformed(ActionEvent e) {
			content.setForeground(Color.BLACK);
			Object element = e.getSource();
			if (element instanceof JButton){	
				switch (e.getActionCommand()) {
				case "Delete":
					if(content.getText().compareToIgnoreCase("") != 0){
						result = 0;
						showResult = false;
						resetText();
					}
					break;
				case "«":
					if(contentText.length() == 0) {resetText(); result = 0; showResult = false;}
					if(content.getText().compareToIgnoreCase("") != 0){
						contentText = deleteLastToken();
						content.setText(contentText);
					}
					break;
				case "÷":
					if(content.getText().compareToIgnoreCase("") != 0 && contentText.compareToIgnoreCase("") != 0 && showResult == false){
						contentText += "÷";
						contentText = (checkLastOperator() ?  swapOperator(2): contentText);
						contentText = (checkThirdOperator() ?  swapOperator(3): contentText);
						content.setText(contentText);
					}
					else if(showResult) {contentText += (showInt ? (int)result+"÷" : result+"÷" ); content.setText(contentText);showResult = false;result=0;}
					else content.setText("");
					break;
				case "×":
					if(content.getText().compareToIgnoreCase("") != 0 && contentText.compareToIgnoreCase("") != 0 && showResult == false){
						contentText += "×";
						contentText = (checkLastOperator() ?  swapOperator(2): contentText);
						contentText = (checkThirdOperator() ?  swapOperator(3): contentText);
						content.setText(contentText);
					}
					else if(showResult) {contentText += (showInt ? (int)result+"×" : result+"×" ); content.setText(contentText);showResult = false;result=0;}
					else content.setText("");
					break;
				case "−":
					if(content.getText().compareToIgnoreCase("") != 0 && contentText.compareToIgnoreCase("") != 0 && showResult == false){
						contentText += "-";
						contentText = (checkLastOperator() ?  swapOperator(2): contentText);
						contentText = (checkDoubleOperators() ?  swapOperator(2): contentText);
						content.setText(contentText);
					}
					else if(showResult) {contentText += (showInt ? (int)result+"-" : result+"-" ); content.setText(contentText);showResult = false;result=0;}
					else content.setText("");
					break;
				case "+":
					if(content.getText().compareToIgnoreCase("") != 0 && contentText.compareToIgnoreCase("") != 0 && showResult == false){
						contentText += "+";
						contentText = (checkLastOperator() ?  swapOperator(2): contentText);
						contentText = (checkThirdOperator() ?  swapOperator(3): contentText);
						content.setText(contentText);
					}
					else if(showResult) {contentText += (showInt ? (int)result+"+" : result+"+" ); content.setText(contentText);showResult = false;result=0;}
					else content.setText("");
					break;
				case ".":
					if(content.getText().compareToIgnoreCase("") != 0 && contentText.compareToIgnoreCase("") != 0){
						contentText += ".";
						contentText = (checkDoublePoint() ?  deleteLastToken(): contentText);
						contentText = (checkDoubleOperators() ?  swapOperator(2): contentText);
						content.setText(contentText);
					}
					else if(showResult) {if(showInt){contentText+= (int)result+".";content.setText(contentText);} else resetText(); showResult = false;result=0;}
					else content.setText("");
					break;
				case "=":
					if(content.getText().compareToIgnoreCase("") != 0 && contentText.compareToIgnoreCase("") != 0){
						deleteLastOperator();
						boolean minus = (contentText.charAt(0) == '-' ? true : false);
						contentText = (minus ? contentText.substring(1,contentText.length()) : contentText);
						result = Calculator.calculate(contentText,minus);
						if(getPhone().getSettingsController().getSettings().isDebugMode())
							System.out.println("jPhone " + phoneNumber + " Calculation: " + contentText +"=" + result + " done");
						content.setForeground(GlobalSettings.calculatorResult());
						int temp = (int)result;
						showInt = (result%temp == 0 ? true:false);
						content.setText((showInt == true ? (int)result+"":(float)result+""));
						contentText = "";
						showResult = true;
					}
					else content.setText("");
					break;
				case "0":
					addNumber("0");
					break;
				case "1":
					addNumber("1");
					break;
				case "2":
					addNumber("2");
					break;
				case "3":
					addNumber("3");
					break;
				case "4":
					addNumber("4");
					break;
				case "5":
					addNumber("5");
					break;
				case "6":
					addNumber("6");
					break;
				case "7":
					addNumber("7");
					break;
				case "8":
					addNumber("8");
					break;
				case "9":
					addNumber("9");
					break;
				}
			}
		}

		private void resetText(){
			contentText = "";
			content.setText(contentText);
		}

		private void addNumber(String number){
			if(showResult){
				contentText = "";
				content.setText(contentText);
				showResult = false;
			}
			contentText += number;
			content.setText(contentText);
		}

		private String deleteLastToken(){
			return contentText.substring(0, contentText.length()-1);
		}

		private String swapOperator(int switchPosition){
			char [] replace = contentText.toCharArray();
			int first = contentText.length()-switchPosition;
			int last = contentText.length()-1;
			char temp = replace[last];
			String result = "";
			temp = replace[last];
			replace[last] = replace[first];
			replace[first] = temp; 
			for (int i = 0; i < replace.length-(switchPosition-1); i++) {
				result += replace[i];
			}
			return result;
		}

		private boolean checkLastOperator(){
			if(contentText.length() < 3) return false;
			String checkThird = contentText.substring(contentText.length()-3);
			if(checkThird.matches("[÷×+]\\-[÷×+]")==true) return false;
			else{
				String temp = contentText.substring(contentText.length()-2);
				if(temp.matches("[÷×+\\-][÷×+]")) return true;
			}
			return false;
		}

		private boolean checkThirdOperator(){
			if(contentText.length() < 4) return false;
			String temp = contentText.substring(contentText.length()-3);
			if(temp.matches("[÷×+]\\-[÷×+]")) return true;
			return false;
		}

		private boolean checkDoubleOperators(){
			if(contentText.length() < 3) return false;
			String temp = contentText.substring(contentText.length()-2);
			if(temp.matches("\\-\\-") || temp.matches("\\-÷") || temp.matches("\\-×") || temp.matches("\\.\\.")) return true;
			return false;
		}

		private boolean checkDoublePoint(){
			String [] temp = contentText.split("[÷|×|+]\\-|[÷|×|\\-|+]");
			int counter = 0;
			for (int i = 0; i < temp.length; i++) {
				for (int j = 0; j < temp[i].length(); j++) {
					counter += (temp[i].charAt(j) == '.' ? 1 : 0);
					if(counter == 2) return true;
				}
				counter = 0;
			}
			return false;
		}

		private void deleteLastOperator(){
			for (int i = 0; i < OPERATORS.length; i++) {
				if(contentText.charAt(contentText.length()-1) == OPERATORS[i]) {
					contentText = deleteLastToken();
					break;
				}
			}
		}
	} 

	ActionEvents actionEvent = new ActionEvents(); 

	public CalculatorView(String phoneNumber){
		super(null);
		this.phoneNumber = phoneNumber;
		this.setOpaque(true);
		this.setBounds(0,0,GlobalSettings.width, GlobalSettings.height);
		this.lightGray = GlobalSettings.calculatorOperators();
		this.bButton = new MatteBorder(1, 0, 0, 1, Color.LIGHT_GRAY);
		this.contentText = "";
		Font fView = GlobalSettings.globalFont(45);
		Font fButton = GlobalSettings.globalFont(25);

		view = new JPanel(null);
		view.setBounds(0, 0, GlobalSettings.width-5, 200);
		content = new JTextField(contentText);
		content.setEditable(false);
		content.setHorizontalAlignment(JTextField.RIGHT);
		content.setBounds(0, 0, GlobalSettings.width-5, 200);
		content.setBackground(Color.WHITE);
		content.setFont(fView);
		content.setBorder(new MatteBorder(0,0,1,0,lightGray));
		view.add(content);

		buttons = new JButton[18];
		for (int i = 0; i < buttons.length; i++) {
			buttons[i] = new JButton(NAMES[i]);
			buttons[i].setFont(fButton);
			buttons[i].setBorder(bButton);
			buttons[i].addActionListener(actionEvent);
			buttons[i].setFocusPainted(false);
			if(i == 0 || i == 1 || i == 5 || i == 9 || i == 13 | i == 17) buttons[i].setBackground(lightGray);
			else buttons[i].setBackground(Color.WHITE);			
		}

		options = new JPanel(new GridBagLayout());
		options.setBounds(0, 200, GlobalSettings.width-5, 415);
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipadx = 50;
		c.ipady = 55;

		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.gridwidth = 3;
		options.add(buttons[0],c);

		c.gridwidth = 1;
		c.gridx = 3;
		c.gridy = 0;
		options.add(buttons[1],c);

		int counter = 2;
		for (int i = 2; i < 6; i++) {
			c.gridy = (i-1);
			for (int j = 0; j < 4; j++) {
				c.gridx = j;
				options.add(buttons[counter],c);
				counter++;
			}
		}
		add(view);
		add(options);	
	}
	
	@Override
	public void addPanel(JPanel p) {
		getPhone().addPanel(p);	
	}

	@Override
	public void removePanel(JPanel p) {}

	@Override
	public JPhone getPhone() {
		return Init.Panel.findJPhone(this.phoneNumber);
	}
}
