/**
 * Autor: Johner Robert
 * Modul: 632-1_jPhone 
 * Datum: 17.05.2015
 * Datei: SMSViewSelect.java
 */
package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.io.Serializable;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import apps.SMSApp;
import enums.AppsName;
import JPhone.Init;
import JPhone.JPhone;
import models.Contact;
import settings.GlobalSettings;
import heredity.Form;
import heredity.PanelManager;
import heredity.SampleView;

/**
 * @author jor
 *
 */
public class SMSViewSelect extends SampleView implements PanelManager,Form{
	private static final long serialVersionUID = 1773495214651182061L;
	private String phoneNumber;
	private JList<Contact> list;

	@SuppressWarnings("unchecked")
	public SMSViewSelect(String phoneNumber) {
		super(GlobalSettings.smsMainColor(), "Select Contact", 103, true, true);
		this.phoneNumber = phoneNumber;
		list = super.list;
		list.setCellRenderer(new Renderer());
		add.setText("");
		if(getPhone().getContactController().getContactList().length != 0)
			setList(getPhone().getContactController().getContactList());
		this.setColor(getPhone().getSettingsController().getSettings().getSmsMainColor());
	}
	
	@SuppressWarnings("rawtypes")
	public class Renderer implements ListCellRenderer, Serializable {
		private static final long serialVersionUID = -6387923718968860931L;
		private JPanel container;
		private JLabel name;
		private JLabel phone;

		public Renderer() {
			container = new JPanel(new BorderLayout());
			container.setBackground(Color.WHITE);
			JPanel p = new JPanel();
			p.setBorder(BorderFactory.createEmptyBorder(0,10,0,30));
			p.setLayout(new BorderLayout(20, 20));
			p.setPreferredSize(new Dimension(GlobalSettings.width-20, 103));
			name = new JLabel();
			name.setFont(GlobalSettings.globalFont(20));
			name.setForeground(GlobalSettings.globalFontColorBlack());
			phone = new JLabel();
			phone.setFont(GlobalSettings.globalFont(20));
			phone.setForeground(GlobalSettings.rendererNumber());
			p.setOpaque(false);
			p.add(name, BorderLayout.CENTER);
			p.add(phone, BorderLayout.EAST);
			container.add(p, BorderLayout.CENTER);
			JPanel bottom = new JPanel();
			bottom.setBackground(GlobalSettings.rendererBottom());
			bottom.setPreferredSize(new Dimension(GlobalSettings.width,1));
			container.add(bottom, BorderLayout.SOUTH);
		}

		@Override
		public Component getListCellRendererComponent(final JList list,
				final Object value, final int index,final boolean isSelected,
				final boolean hasFocus) {
			if(index % 2 == 0) {
				container.setBackground(GlobalSettings.rendererList2());
			} else {
				container.setBackground(Color.white);
			}
			Contact c = null;
			if(value instanceof Contact) {
				c = (Contact) value;
			}
			if(isSelected) {
				container.setBackground(GlobalSettings.smsMainColor());
				name.setForeground(GlobalSettings.globalFontColor());
				phone.setForeground(GlobalSettings.globalFontColor());	
				if(GlobalSettings.debugMode)
					System.out.println("jPhone " + getPhone().getPhoneNumber() +" "+ c.toString() +" selected");
				getPhone().getSMSController().addConversation(c);
				getPhone().removeLastPanel();
				((SMSView)getPhone().getCurrentPanel()).refresh();
				int i = getPhone().findApp(AppsName.SMS.getName());
				if(i != -1)
					getPhone().appList.get(i).addView(new SMSViewConversation(phoneNumber,c));
				if(GlobalSettings.debugMode)
					System.out.println("jPhone " + getPhone().getPhoneNumber() +" Conversation started with " + c.getLname()+" "+c.getFname());
				getPhone().addPanel(((SMSApp)(getPhone().appList.get(i))).getView(getPhone().appList.get(i).getViews().size()-1));
				((SMSViewConversation)getPhone().getCurrentPanel()).setFocusOnText();
				list.clearSelection();
			} else {
				name.setForeground(GlobalSettings.globalFontColorBlack());
				phone.setForeground(GlobalSettings.rendererNumber());
			}
			if(c != null) {
				c = (Contact) value;
				name.setText(c.getLname()+" "+c.getFname());
				phone.setText(c.getPhoneNumber());
			}			
			return container;
		}
	}

	public void setList(Contact [] c){
		list.setListData(c);
	}

	@Override
	public void resetText() {	
		getPhone().appList.get(1).getViews().remove(getPhone().appList.get(1).getViews().size()-1);
	}

	@Override
	public void add() {return;}

	@Override
	public void search(String s) {
		list.setListData(getPhone().getContactController().getContactModel().search(s));
	}

	@Override
	public void refresh() {
		list.setListData(getPhone().getContactController().getContactList());
	}

	@Override
	public void remove() {}

	@Override
	public void addPanel(JPanel p) {
		getPhone().addPanel(p);	
	}

	@Override
	public void removePanel(JPanel p) {}

	@Override
	public JPhone getPhone() {
		return Init.Panel.findJPhone(this.phoneNumber);
	}
}
