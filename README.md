# jPhone

<p align="center">
 <img src="jPhone.png?raw=true" alt="jPhone"/>
</p>

A JAVA mobile phone in which you can play Sodoku, writes messages, manages contacts and make calculations.

 
   [git-lab]: <https://gitlab.com/jor93>
   [website-aboutme]: <https://gitlab.com/jor93/java-jPhone>
  
*made with ♥ by jor*
